create or replace package         RASDC2_HTML is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_HTML generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  function changes(p_log out varchar2) return varchar2;

procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
);
end;

/
create or replace package body         RASDC2_HTML is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_HTML generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);  RECNUMB1                      number := 1
;
  RECNUMB10                     varchar2(4000);  RECNUMP                       number := 1
;
  ACTION                        varchar2(4000);  GBUTTONCLR                    varchar2(4000) := 'GBUTTONCLR'
;  PAGE                          number := 0
;  GBUTTONBCK                    varchar2(4000) := '<<'
;  GBUTTONFWD                    varchar2(4000) := '>>'
;
  LANG                          varchar2(4000);
  PFORMID                       varchar2(4000);
  PFORM                         varchar2(4000);  GBUTTONSRC                    varchar2(4000) := RASDI_TRNSLT.text('Search',LANG)
;  GBUTTONSAVE                   varchar2(4000) := RASDI_TRNSLT.text('Save',LANG)
;
  GBUTTONSAVE#SET                set_type;  GBUTTONCOMPILE                varchar2(4000) := RASDI_TRNSLT.text('Compile',LANG)
;
  GBUTTONCOMPILE#SET             set_type;  GBUTTONRES                    varchar2(4000) := RASDI_TRNSLT.text('Reset',LANG)
;
  GBUTTONRES#SET                 set_type;  GBUTTONPREV                   varchar2(4000) := RASDI_TRNSLT.text('Preview',LANG)
;
  GBUTTONPREV#SET                set_type;  GBUTTONDELEL                  varchar2(4000) := RASDI_TRNSLT.text('Delete all HTML elements', lang)
;
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
  VUSER                         varchar2(4000);
  VLOB                          varchar2(4000);
  COMPID                        number;
  HINTCONTENT                   varchar2(4000);
  POLJE                         varchar2(4000);
  PFELEMENTID                   ctab;
  PTCLEVEL                      ctab;
  PFILTER                       ctab;
  PPF                           ctab;
  PPF#SET                        stab;
  PGBTNSRC                      ctab;
  PUPLOAD                       ctab;
  PDOWNLOAD                     ctab;
  B1RS                          ctab;
  B1RID                         rtab;
  B1ELEMENTID                   ntab;
  B1ATTS                        ctab;
  B1ATTS#SET                     stab;
  B1ELEMENT                     ctab;
  B1HIDDENYN                    ctab;
  B1SOURCE                      ctab;
  B1REFERENCE                   ctab;
  B1ELEMENTX                    ctab;
  B1SOURCEX                     ctab;
  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := '/* Change LOG:

20230301 - Created new 2 version

20170105 - Modified filter on Pages page

*/';

    return version;

 end;
function showLabel(plabel varchar2, pcolor varchar2 default 'U', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,' ',''),'.',''),lang,replace(this_form,'2','')||'_DIALOG');

end if;



if pcolor is null then



return v__;



else



return '<font color="'||pcolor||'">'||v__||'</font>';



end if;





end;
procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text('User has no privileges to save data!', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, 'COMPILE_S', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text('From is not generated.', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           'begin ' || v_server || '.c_debug := false;'|| v_server || '.form(' || PFORMID ||

                           ',''' || lang || ''');end;',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text('From is generated.', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || '<br/> - '||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text('Form is generated with compilation error. Check your code.', lang)||'('||sqlerrm||')';



            else

            sporocilo := RASDI_TRNSLT.text('Form is NOT generated - internal RASD error.', lang) || '('||sqlerrm||')<br>'||

                         RASDI_TRNSLT.text('To debug run: ', lang) || 'begin ' || v_server || '.form(' || PFORMID ||

                         ',''' || lang || ''');end;' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, 'COMPILE_E', pcompid);

end;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop;
       end;
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASDC2_HTML',v_clob, systimestamp, '' );
       end;
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end;
     function form_js return clob is
          v_out clob;
       begin
        v_out :=  '
$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

//   setShowHideDiv("B30_DIV", true);

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)





ShowLOVIcon();



HighLightRow(''showrow'', ''lightcoral'')



 });


$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("'|| RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) ||'");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
        ';
        return v_out;
       end;
     function form_css return clob is
          v_out clob;
       begin
        v_out :=  '

        ';
        return v_out;
       end;
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;

 procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr);
 procedure on_session;
 procedure formgen_js;

function openLOV(
  p_lov varchar2,
  p_value varchar2
) return lovtab__ is
  name_array   owa.vc_arr;
  value_array  owa.vc_arr;
begin
  name_array(1) := 'PLOV';
  value_array(1) := p_lov;
  name_array(2) := 'PID';
  value_array(2) := p_value;
  name_array(3) := 'CALL';
  value_array(3) := 'PLSQL';
  openLOV(name_array, value_array);
  return lov__;
end;
procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
  num_entries number := name_array.count;
cursor c_lov$elementi(p_id varchar2) is
--<lovsql formid="102" linkid="lov$elementi">
select id, label from (                  select -1 orderby, '' label, to_number(null) id

                    from dual

                  union

                  select orderby,

                          decode(nameid, null, element, nameid) label,

                          elementid id

                    from RASD_ELEMENTS

                   where formid = PFORMID

                     and (element in ('HTML_', 'HEAD_', 'BODY_') or

                         nameid is not null)

                   order by orderby ) where upper(id) like '%'||upper(p_id)||'%' or upper(label) like '%'||upper(p_id)||'%'
--</lovsql>
;
cursor c_lov$tclevel(p_id varchar2) is
--<lovsql formid="102" linkid="lov$tclevel">
select id, label from (                 select 1 vr, '' label, '' id

                    from dual

                  union

                  select 2 vr,

                          RASDI_TRNSLT.text('Form - blocks', lang) label,

                          'FB' id

                    from dual

                  union

                  select 3 vr,

                          RASDI_TRNSLT.text('Blocks - fields', lang) label,

                          'BP' id

                    from dual

                  union

                  select 4 vr, '1 level' label, '1' id

                    from dual

                  union

                  select 5 vr, '2 level' label, '2' id

                    from dual

                  union

                  select 6 vr, '3 level' label, '3' id

                    from dual

                  union

                  select 7 vr, '4 level' label, '4' id

                    from dual

                  union

                  select 8 vr, '5 level' label, '5' id

                    from dual

                  union

                  select 9 vr, '6 level' label, '6' id

                    from dual

                   order by vr ) where upper(id) like '%'||upper(p_id)||'%' or upper(label) like '%'||upper(p_id)||'%'
--</lovsql>
;
TYPE pLOVType IS RECORD (
output varchar2(500),
p1 varchar2(200)
);
  TYPE tab_pLOVType IS TABLE OF pLOVType INDEX BY BINARY_INTEGER;
  v_lov tab_pLOVType;
  v_lovf tab_pLOVType;
  v_counter number := 1;
  v_description varchar2(100);
  p_lov varchar2(100);
  p_nameid varchar2(100);
  p_id varchar2(100);
  v_output boolean;
  v_call varchar2(10);
  v_hidden_fields varchar2(32000) := '';
  v_opener_tekst  varchar2(32000) := '';
  RESTRESTYPE varchar2(10);
begin
  on_submit(name_array, value_array);
  for i in 1..num_entries loop
    if name_array(i) = 'PLOV' then p_lov := value_array(i);
    elsif name_array(i) = 'FIN' then p_nameid := value_array(i);
    elsif name_array(i) = 'PID' then p_id := value_array(i);
    elsif upper(name_array(i)) = 'CALL' then v_call := value_array(i);
    elsif upper(name_array(i)) = upper('RESTRESTYPE') then RESTRESTYPE := value_array(i);
    else
      if name_array(i) not in ('LOVlist') then
        v_hidden_fields := v_hidden_fields||'<input type="hidden" name="'||name_array(i)||'" value="'||value_array(i)||'" />';
      end if;
    end if;
  end loop;
    if v_call not in ('PLSQL','REST') then
      on_session;
    end if;
  if lower(p_lov) = lower('lov$elementi') then
    v_description := 'Elements';
    for r in c_lov$elementi(p_id) loop
        v_lov(v_counter).p1 := r.id;
        v_lov(v_counter).output := r.label;
        v_counter := v_counter + 1;
    end loop;
    v_counter := v_counter - 1;
        if 1=2 then null;
        end if;
  elsif lower(p_lov) = lower('lov$tclevel') then
    v_description := 'LEVEL';
    for r in c_lov$tclevel(p_id) loop
        v_lov(v_counter).p1 := r.id;
        v_lov(v_counter).output := r.label;
        v_counter := v_counter + 1;
    end loop;
    v_counter := v_counter - 1;
        if 1=2 then null;
        end if;
  elsif lower(p_lov) = lower('lov$source') then
    v_description := 'Source';
        v_lov(1).output := '';
        v_lov(1).p1 := '';
        v_lov(2).output := ''|| RASDI_TRNSLT.text('Eng.', lang) ||'';
        v_lov(2).p1 := 'G';
        v_lov(3).output := ''|| RASDI_TRNSLT.text('Chng.', lang) ||'';
        v_lov(3).p1 := 'V';
        v_lov(4).output := ''|| RASDI_TRNSLT.text('Ref.', lang) ||'';
        v_lov(4).p1 := 'R';
        v_counter := 4;
      if p_id is not null then
      for i in 1..v_lov.count loop
       if instr( v_lov(i).output , p_id)+ instr( v_lov(i).p1 , p_id) > 0 then
        v_lovf( nvl(v_lovf.count+1,1) ).output := v_lov(i).output;
        v_lovf( v_lovf.count ).p1 := v_lov(i).p1;
       end if;
      end loop;
      v_lov := v_lovf;
      v_counter := v_lov.count;
      v_lovf.delete;
    end if;
        if 1=2 then null;
        end if;
  elsif lower(p_lov) = lower('link$CHKBXD') then
    v_description := 'CHKBXD';
        v_lov(1).output := 'N';
        v_lov(1).p1 := 'N';
        v_lov(2).output := 'Y';
        v_lov(2).p1 := 'Y';
        v_counter := 2;
        if 1=2 then null;
        end if;
  else
   return;
  end if;
if v_call = 'PLSQL' then
  lov__.delete;
  for i in 1..v_lov.count loop
   lov__(i).id := v_lov(i).p1;
   lov__(i).label := v_lov(i).output;
  end loop;
elsif v_call = 'REST' then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE);
    OWA_UTIL.http_header_close;
 htp.p('<?xml version="1.0" encoding="UTF-8"?>
<openLOV LOV="'||p_lov||'" filter="'||p_id||'">');
 htp.p('<result>');
 for i in 1..v_counter loop
 htp.p('<element><code>'||v_lov(i).p1||'</code><description>'||v_lov(i).output||'</description></element>');
 end loop;
 htp.p('</result></openLOV>');
else
    OWA_UTIL.mime_header('application/json', FALSE);
    OWA_UTIL.http_header_close;
 htp.p('{"openLOV":{"@LOV":"'||p_lov||'","@filter":"'||p_id||'",' );
 htp.p('"result":[');
 for i in 1..v_counter loop
  if i = 1 then
 htp.p('{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  else
 htp.p(',{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  end if;
 end loop;
 htp.p(']}}');
end if;
else
 htp.p('
<html>');
    htp.prn('<head>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head></head>
    ');
 htp.bodyOpen('','');
htp.p('
<script language="JavaScript">
        $(function() {
        document.getElementById("PID").select();
        });
   function closeLOV() {
     this.close();
   }
   function selectLOV() {
     var value = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].value;
     var tekst = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].text;
     window.opener.'||p_nameid||'.value = value;
     '||v_opener_tekst||'
     event = new Event(''change'');
     window.opener.'||p_nameid||'.dispatchEvent(event);
     ');
htp.p('this.close ();
   }
  with (document) {
  if (screen.availWidth < 900){
    moveTo(-4,-4)}
  }
</script>');
 htp.p('<div class="rasdLovName">'||v_description||'</div>');
 htp.formOpen(curl=>'!RASDC2_HTML.openLOV',
                 cattributes=>'name="'||p_lov||'"');
 htp.p('<input type="hidden" name="PLOV" value="'||p_lov||'">');
 htp.p('<input type="hidden" name="FIN" value="'||p_nameid||'">');
 htp.p(v_hidden_fields);
 htp.p('<div class="rasdLov" align="center"><center>');
 htp.p('Filter:<input type="text" id="PID" autofocus="autofocus" name="PID" value="'||p_id||'" ></BR><input type="submit" class="rasdButton" value="Search"><input class="rasdButton" type="button" value="Clear" onclick="document.'||p_lov||'.PID.value=''''; document.'||p_lov||'.submit();"></BR>');
 htp.formselectOpen('LOVlist',cattributes=>'size=15 width="100%"');
 for i in 1..v_counter loop
  if i = 1 then -- fokus na prvem
    htp.formSelectOption(cvalue=>v_lov(i).output,cselected=>1,Cattributes => 'value="'||v_lov(i).p1||'"');
  else
    htp.formSelectOption(cvalue=>v_lov(i).output,Cattributes => 'value="'||v_lov(i).p1||'"');
  end if;
 end loop;
 htp.formselectClose;
 htp.p('');
 htp.line;
 htp.p('<input type="button" class="rasdButton" value="Select and Confirm" onClick="selectLOV();">');
 htp.p('<input type="button" class="rasdButton" value="Close" onClick="closeLOV();">');
 htp.p('</center></div>');
 htp.p('</form>');
 htp.p('</body>');
 htp.p('</html>');
end if;
end;
  function version return varchar2 is
  begin
   return 'v.1.1.20240228090934';
  end;
  function this_form return varchar2 is
  begin
   return 'RASDC2_HTML';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version );
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
   if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then null;
  if ACTION = GBUTTONCLR then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PFELEMENTID'', '''' ); ';
  else
    if length( PFELEMENTID(i__)) < 512 then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PFELEMENTID'', '''||replace(PFELEMENTID(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PFELEMENTID'', '''' ); ';
     end if;
 end if;
  if ACTION = GBUTTONCLR then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PTCLEVEL'', '''' ); ';
  else
    if length( PTCLEVEL(i__)) < 512 then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PTCLEVEL'', '''||replace(PTCLEVEL(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PTCLEVEL'', '''' ); ';
     end if;
 end if;
    end if;
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else
declare vc varchar2(2000); begin
null;
if PFELEMENTID(i__) is null then vc := rasd_client.sessionGetValue('PFELEMENTID'); PFELEMENTID(i__)  := vc;  end if;
if PTCLEVEL(i__) is null then vc := rasd_client.sessionGetValue('PTCLEVEL'); PTCLEVEL(i__)  := vc;  end if;
exception when others then  null; end;
  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,RECNUMB1 number PATH '$.recnumb1'
  ,RECNUMB10 varchar2(4000) PATH '$.recnumb10'
  ,RECNUMP number PATH '$.recnump'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE number PATH '$.page'
  ,GBUTTONBCK varchar2(4000) PATH '$.gbuttonbck'
  ,GBUTTONFWD varchar2(4000) PATH '$.gbuttonfwd'
  ,LANG varchar2(4000) PATH '$.lang'
  ,PFORMID varchar2(4000) PATH '$.pformid'
  ,GBUTTONSAVE varchar2(4000) PATH '$.gbuttonsave'
  ,GBUTTONCOMPILE varchar2(4000) PATH '$.gbuttoncompile'
  ,GBUTTONRES varchar2(4000) PATH '$.gbuttonres'
  ,GBUTTONPREV varchar2(4000) PATH '$.gbuttonprev'
  ,GBUTTONDELEL varchar2(4000) PATH '$.gbuttondelel'
  ,HINTCONTENT varchar2(4000) PATH '$.hintcontent'
  ,POLJE varchar2(4000) PATH '$.polje'
)) jt ) loop
 if instr(RESTREQUEST,'recnumb1') > 0 then RECNUMB1 := r__.RECNUMB1; end if;
 if instr(RESTREQUEST,'recnumb10') > 0 then RECNUMB10 := r__.RECNUMB10; end if;
 if instr(RESTREQUEST,'recnump') > 0 then RECNUMP := r__.RECNUMP; end if;
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
 if instr(RESTREQUEST,'gbuttonbck') > 0 then GBUTTONBCK := r__.GBUTTONBCK; end if;
 if instr(RESTREQUEST,'gbuttonfwd') > 0 then GBUTTONFWD := r__.GBUTTONFWD; end if;
 if instr(RESTREQUEST,'lang') > 0 then LANG := r__.LANG; end if;
 if instr(RESTREQUEST,'pformid') > 0 then PFORMID := r__.PFORMID; end if;
 if instr(RESTREQUEST,'gbuttonsave') > 0 then GBUTTONSAVE := r__.GBUTTONSAVE; end if;
 if instr(RESTREQUEST,'gbuttoncompile') > 0 then GBUTTONCOMPILE := r__.GBUTTONCOMPILE; end if;
 if instr(RESTREQUEST,'gbuttonres') > 0 then GBUTTONRES := r__.GBUTTONRES; end if;
 if instr(RESTREQUEST,'gbuttonprev') > 0 then GBUTTONPREV := r__.GBUTTONPREV; end if;
 if instr(RESTREQUEST,'gbuttondelel') > 0 then GBUTTONDELEL := r__.GBUTTONDELEL; end if;
 if instr(RESTREQUEST,'hintcontent') > 0 then HINTCONTENT := r__.HINTCONTENT; end if;
 if instr(RESTREQUEST,'polje') > 0 then POLJE := r__.POLJE; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.p' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,PFELEMENTID varchar2(4000) PATH '$.pfelementid'
  ,PTCLEVEL varchar2(4000) PATH '$.ptclevel'
  ,PGBTNSRC varchar2(4000) PATH '$.pgbtnsrc'
  ,PUPLOAD varchar2(4000) PATH '$.pupload'
  ,PDOWNLOAD varchar2(4000) PATH '$.pdownload'
)) jt ) loop
 if instr(RESTREQUEST,'pfelementid') > 0 then PFELEMENTID(i__) := r__.PFELEMENTID; end if;
 if instr(RESTREQUEST,'ptclevel') > 0 then PTCLEVEL(i__) := r__.PTCLEVEL; end if;
 if instr(RESTREQUEST,'pgbtnsrc') > 0 then PGBTNSRC(i__) := r__.PGBTNSRC; end if;
 if instr(RESTREQUEST,'pupload') > 0 then PUPLOAD(i__) := r__.PUPLOAD; end if;
 if instr(RESTREQUEST,'pdownload') > 0 then PDOWNLOAD(i__) := r__.PDOWNLOAD; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b1[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B1RS varchar2(4000) PATH '$.b1rs'
  ,B1RID varchar2(4000) PATH '$.b1rid'
  ,B1ELEMENTID number PATH '$.b1elementid'
  ,B1ATTS varchar2(4000) PATH '$.b1atts'
  ,B1HIDDENYN varchar2(4000) PATH '$.b1hiddenyn'
  ,B1SOURCE varchar2(4000) PATH '$.b1source'
  ,B1ELEMENTX varchar2(4000) PATH '$.b1elementx'
)) jt ) loop
if r__.x__ is not null then
 if instr(RESTREQUEST,'b1rs') > 0 then B1RS(i__) := r__.B1RS; end if;
 if instr(RESTREQUEST,'b1rid') > 0 then B1RID(i__) := r__.B1RID; end if;
 if instr(RESTREQUEST,'b1elementid') > 0 then B1ELEMENTID(i__) := r__.B1ELEMENTID; end if;
 if instr(RESTREQUEST,'b1atts') > 0 then B1ATTS(i__) := r__.B1ATTS; end if;
 if instr(RESTREQUEST,'b1hiddenyn') > 0 then B1HIDDENYN(i__) := r__.B1HIDDENYN; end if;
 if instr(RESTREQUEST,'b1source') > 0 then B1SOURCE(i__) := r__.B1SOURCE; end if;
 if instr(RESTREQUEST,'b1elementx') > 0 then B1ELEMENTX(i__) := r__.B1ELEMENTX; end if;
i__ := i__ + 1;
end if;
end loop;
  end;
  function validate_submit(v_text varchar2) return varchar2 is
    v_outt varchar2(32000) := v_text;
  begin
    if instr(v_outt,'"') > 0 then v_outt := replace(v_outt,'"','&quot;');
    elsif instr(v_outt,'%22') > 0 then v_outt := replace(v_outt,'%22','&quot;');
    elsif instr(lower(v_outt),'<script') > 0 then v_outt := replace(v_outt,'<script','&lt;script');
    end if;
    return v_outt;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB1') then RECNUMB1 := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB10') then RECNUMB10 := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMP') then RECNUMP := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCLR') then GBUTTONCLR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONBCK') then GBUTTONBCK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONFWD') then GBUTTONFWD := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('LANG') then LANG := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORMID') then PFORMID := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORM') then PFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSAVE') then GBUTTONSAVE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCOMPILE') then GBUTTONCOMPILE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONPREV') then GBUTTONPREV := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONDELEL') then GBUTTONDELEL := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VUSER') then VUSER := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VLOB') then VLOB := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('COMPID') then COMPID := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('HINTCONTENT') then HINTCONTENT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('POLJE') then POLJE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFELEMENTID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PFELEMENTID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PTCLEVEL_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PTCLEVEL(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFILTER_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PFILTER(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PF_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PPF(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSRC_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGBTNSRC(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PUPLOAD_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PUPLOAD(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PDOWNLOAD_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PDOWNLOAD(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1RS_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1RS(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1RID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1RID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := chartorowid(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ELEMENTID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1ELEMENTID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ATTS_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1ATTS(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ELEMENT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1ELEMENT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1HIDDENYN_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1HIDDENYN(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1SOURCE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1SOURCE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1REFERENCE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1REFERENCE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ELEMENTX_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1ELEMENTX(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1SOURCEX_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1SOURCEX(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFELEMENTID') and PFELEMENTID.count = 0 and value_array(i__) is not null then
        PFELEMENTID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PTCLEVEL') and PTCLEVEL.count = 0 and value_array(i__) is not null then
        PTCLEVEL(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFILTER') and PFILTER.count = 0 and value_array(i__) is not null then
        PFILTER(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PF') and PPF.count = 0 and value_array(i__) is not null then
        PPF(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSRC') and PGBTNSRC.count = 0 and value_array(i__) is not null then
        PGBTNSRC(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PUPLOAD') and PUPLOAD.count = 0 and value_array(i__) is not null then
        PUPLOAD(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PDOWNLOAD') and PDOWNLOAD.count = 0 and value_array(i__) is not null then
        PDOWNLOAD(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1RS') and B1RS.count = 0 and value_array(i__) is not null then
        B1RS(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1RID') and B1RID.count = 0 and value_array(i__) is not null then
        B1RID(1) := chartorowid(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ELEMENTID') and B1ELEMENTID.count = 0 and value_array(i__) is not null then
        B1ELEMENTID(1) := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ATTS') and B1ATTS.count = 0 and value_array(i__) is not null then
        B1ATTS(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ELEMENT') and B1ELEMENT.count = 0 and value_array(i__) is not null then
        B1ELEMENT(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1HIDDENYN') and B1HIDDENYN.count = 0 and value_array(i__) is not null then
        B1HIDDENYN(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1SOURCE') and B1SOURCE.count = 0 and value_array(i__) is not null then
        B1SOURCE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1REFERENCE') and B1REFERENCE.count = 0 and value_array(i__) is not null then
        B1REFERENCE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ELEMENTX') and B1ELEMENTX.count = 0 and value_array(i__) is not null then
        B1ELEMENTX(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1SOURCEX') and B1SOURCEX.count = 0 and value_array(i__) is not null then
        B1SOURCEX(1) := validate_submit(value_array(i__));
      end if;
    end loop;
-- organize records
declare
v_last number :=
B1RID
.last;
v_curr number :=
B1RID
.first;
i__ number;
begin
 if v_last <>
B1RID
.count then
   v_curr :=
B1RID
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B1RS.exists(v_curr) then B1RS(i__) := B1RS(v_curr); end if;
      if B1RID.exists(v_curr) then B1RID(i__) := B1RID(v_curr); end if;
      if B1ELEMENTID.exists(v_curr) then B1ELEMENTID(i__) := B1ELEMENTID(v_curr); end if;
      if B1ATTS.exists(v_curr) then B1ATTS(i__) := B1ATTS(v_curr); end if;
      if B1ELEMENT.exists(v_curr) then B1ELEMENT(i__) := B1ELEMENT(v_curr); end if;
      if B1HIDDENYN.exists(v_curr) then B1HIDDENYN(i__) := B1HIDDENYN(v_curr); end if;
      if B1SOURCE.exists(v_curr) then B1SOURCE(i__) := B1SOURCE(v_curr); end if;
      if B1REFERENCE.exists(v_curr) then B1REFERENCE(i__) := B1REFERENCE(v_curr); end if;
      if B1ELEMENTX.exists(v_curr) then B1ELEMENTX(i__) := B1ELEMENTX(v_curr); end if;
      if B1SOURCEX.exists(v_curr) then B1SOURCEX(i__) := B1SOURCEX(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B1RID
.NEXT(v_curr);
   END LOOP;
      B1RS.DELETE(i__ , v_last);
      B1RID.DELETE(i__ , v_last);
      B1ELEMENTID.DELETE(i__ , v_last);
      B1ATTS.DELETE(i__ , v_last);
      B1ELEMENT.DELETE(i__ , v_last);
      B1HIDDENYN.DELETE(i__ , v_last);
      B1SOURCE.DELETE(i__ , v_last);
      B1REFERENCE.DELETE(i__ , v_last);
      B1ELEMENTX.DELETE(i__ , v_last);
      B1SOURCEX.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B1RS.count > v_max then v_max := B1RS.count; end if;
    if B1RID.count > v_max then v_max := B1RID.count; end if;
    if B1ELEMENTID.count > v_max then v_max := B1ELEMENTID.count; end if;
    if B1ATTS.count > v_max then v_max := B1ATTS.count; end if;
    if B1ELEMENT.count > v_max then v_max := B1ELEMENT.count; end if;
    if B1HIDDENYN.count > v_max then v_max := B1HIDDENYN.count; end if;
    if B1SOURCE.count > v_max then v_max := B1SOURCE.count; end if;
    if B1REFERENCE.count > v_max then v_max := B1REFERENCE.count; end if;
    if B1ELEMENTX.count > v_max then v_max := B1ELEMENTX.count; end if;
    if B1SOURCEX.count > v_max then v_max := B1SOURCEX.count; end if;
    if v_max = 0 then v_max := 50; end if;
    for i__ in 1..v_max loop
      if not B1RS.exists(i__) then
        B1RS(i__) := null;
      end if;
      if not B1RID.exists(i__) then
        B1RID(i__) := null;
      end if;
      if not B1ELEMENTID.exists(i__) then
        B1ELEMENTID(i__) := to_number(null);
      end if;
      if not B1ATTS.exists(i__) then
        B1ATTS(i__) := null;
      end if;
      if not B1ATTS#SET.exists(i__) then
        B1ATTS#SET(i__).visible := true;
      end if;
      if not B1ELEMENT.exists(i__) then
        B1ELEMENT(i__) := null;
      end if;
      if not B1HIDDENYN.exists(i__) or B1HIDDENYN(i__) is null then
        B1HIDDENYN(i__) := 'N';
      end if;
      if not B1SOURCE.exists(i__) then
        B1SOURCE(i__) := null;
      end if;
      if not B1REFERENCE.exists(i__) then
        B1REFERENCE(i__) := null;
      end if;
      if not B1ELEMENTX.exists(i__) then
        B1ELEMENTX(i__) := null;
      end if;
      if not B1SOURCEX.exists(i__) then
        B1SOURCEX(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if PFELEMENTID.count > v_max then v_max := PFELEMENTID.count; end if;
    if PTCLEVEL.count > v_max then v_max := PTCLEVEL.count; end if;
    if PFILTER.count > v_max then v_max := PFILTER.count; end if;
    if PPF.count > v_max then v_max := PPF.count; end if;
    if PGBTNSRC.count > v_max then v_max := PGBTNSRC.count; end if;
    if PUPLOAD.count > v_max then v_max := PUPLOAD.count; end if;
    if PDOWNLOAD.count > v_max then v_max := PDOWNLOAD.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not PFELEMENTID.exists(i__) then
        PFELEMENTID(i__) := null;
      end if;
      if not PTCLEVEL.exists(i__) then
        PTCLEVEL(i__) := null;
      end if;
      if not PFILTER.exists(i__) then
        PFILTER(i__) := null;
      end if;
      if not PPF.exists(i__) then
        PPF(i__) := null;
      end if;
      if not PPF#SET.exists(i__) then
        PPF#SET(i__).visible := true;
      end if;
      if not PGBTNSRC.exists(i__) then
        PGBTNSRC(i__) := gbuttonsrc;
      end if;
      if not PUPLOAD.exists(i__) then
        PUPLOAD(i__) := 'rasdc_files.showfile?pfile=pict/gumbhtmlu.jpg';
      end if;
      if not PDOWNLOAD.exists(i__) then
        PDOWNLOAD(i__) := 'rasdc_files.showfile?pfile=pict/gumbhtmld.jpg';
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin
--<POST_SUBMIT formid="102" blockid="">
----put procedure in the begining of trigger;

post_submit_template;


--</POST_SUBMIT>
    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_P(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        PFELEMENTID(i__) := null;
        PTCLEVEL(i__) := null;
        PFILTER(i__) := null;
        PPF(i__) := null;
        PGBTNSRC(i__) := gbuttonsrc;
        PUPLOAD(i__) := 'rasdc_files.showfile?pfile=pict/gumbhtmlu.jpg';
        PDOWNLOAD(i__) := 'rasdc_files.showfile?pfile=pict/gumbhtmld.jpg';
        PPF#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_B1(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 50;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + -50;
 if pstart = 0 then k__ := k__ +
B1RID
.count(); end if;
      else
       if i__ > 50 then  k__ := i__ + -50;
       else k__ := -50 + 50;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and -50 + 50=0 then j__ := 0; k__:= 50; end if;
      for i__ in 1..j__ loop
      begin
      B1RS(i__) := B1RS(i__);
      exception when others then
        B1RS(i__) := null;

      end;
      begin
      B1ATTS#SET(i__) := B1ATTS#SET(i__);
      exception when others then
        B1ATTS#SET(i__).visible := true;

      end;
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B1RS(i__) := null;
        B1RID(i__) := null;
        B1ELEMENTID(i__) := null;
        B1ATTS(i__) := null;
        B1ELEMENT(i__) := null;
        B1HIDDENYN(i__) := null;
        B1SOURCE(i__) := null;
        B1REFERENCE(i__) := null;
        B1ELEMENTX(i__) := null;
        B1SOURCEX(i__) := null;
        B1ATTS#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_form is
  begin
    RECNUMB1 := 1;
    RECNUMB10 := null;
    RECNUMP := 1;
    GBUTTONCLR := 'GBUTTONCLR';
    PAGE := 0;
    GBUTTONBCK := '<<';
    GBUTTONFWD := '>>';
    LANG := null;
    PFORMID := null;
    PFORM := null;
    GBUTTONSRC := RASDI_TRNSLT.text('Search',LANG);
    GBUTTONSAVE := RASDI_TRNSLT.text('Save',LANG);
    GBUTTONCOMPILE := RASDI_TRNSLT.text('Compile',LANG);
    GBUTTONRES := RASDI_TRNSLT.text('Reset',LANG);
    GBUTTONPREV := RASDI_TRNSLT.text('Preview',LANG);
    GBUTTONDELEL := RASDI_TRNSLT.text('Delete all HTML elements', lang);
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
    VUSER := null;
    VLOB := null;
    COMPID := null;
    HINTCONTENT := null;
    POLJE := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_P(0);
    pclear_B1(0);

  null;
  end;
  procedure pselect_P is
    i__ pls_integer;
  begin
      pclear_P(PFELEMENTID.count);
  null; end;
  procedure pselect_B1 is
    i__ pls_integer;
  begin
      B1RID.delete;
      B1ELEMENTID.delete;
      B1ATTS.delete;
      B1ELEMENT.delete;
      B1HIDDENYN.delete;
      B1SOURCE.delete;
      B1REFERENCE.delete;
      B1ELEMENTX.delete;
      B1SOURCEX.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
select rowid rid,

                 elementid,

				 '*' atts,

                 '<font size="-3">' ||

                  substr('1---2---3---4---5---6---7---8---9--10' ||

                         '--11--12--13--14--15--16--17--18--19--20' ||

                         '--21--22--23--24--25--26--27--28--29--30',

                         1,

                         ((level - 1) * 4 + 1)) || '</font>   <B>' ||

                  element || '  ' ||

                  decode(nvl(nameid, id), null, '', ' <font color=blue>') ||

                  nvl(nameid, id) ||

                  decode(nvl(nameid, id), null, '', '</font>') || '</B>' || '(' ||

                  elementid || ') ' element,

                 hiddenyn,

                 nvl(source, '?'),

                 rform || '.' || rid referenca,

                 element || '(' || nameid || ')' elementx,

                 decode( (select count(*) from rasd_attributes u where u.formid=r.formid and u.elementid=r.elementid and nvl(u.source,'G') <> 'G'), 0 , '' , 'X'  )||

				 decode(nvl(r.source,'G'),'G','','X') sourcex

            from RASD_ELEMENTS r

           where formid = PFORMID

             and ((PTCLEVEL(1) = 'FB' and element in ('FORM_', 'TR_', 'P_') and

                 nameid is not null) or

                 (PTCLEVEL(1) = 'BP' and

                 ((element in ('TR_', 'P_') and nameid is not null) or

                 (element not in ('TR_', 'P_') and nameid is not null))) or

                 (PTCLEVEL(1) = '1' and level <= 2) or

                 (PTCLEVEL(1) = '2' and level <= 3) or

                 (PTCLEVEL(1) = '3' and level <= 4) or

                 (PTCLEVEL(1) = '4' and level <= 5) or PTCLEVEL(1) is null)

          connect by formid = prior formid

                 and pelementid = prior elementid

           start with formid = PFORMID

                  and ((PFELEMENTID(1) is null and pelementid = 0) or

                      (elementid = PFELEMENTID(1)))

           order siblings by orderby;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B1RID(i__)
           ,B1ELEMENTID(i__)
           ,B1ATTS(i__)
           ,B1ELEMENT(i__)
           ,B1HIDDENYN(i__)
           ,B1SOURCE(i__)
           ,B1REFERENCE(i__)
           ,B1ELEMENTX(i__)
           ,B1SOURCEX(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  nvl(RECNUMB1,1) then
            B1RS(i__) := 'U';
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.

--<post_select formid="102" blockid="B1">
if B1SOURCEX(i__) is not null then



B1ATTS#SET(i__).custom := 'class="showrow"';



end if;
--</post_select>
            exit when i__ =50;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  nvl(RECNUMB1,1) then
          B1RS.delete(1);
          B1RID.delete(1);
          B1ELEMENTID.delete(1);
          B1ATTS.delete(1);
          B1ELEMENT.delete(1);
          B1HIDDENYN.delete(1);
          B1SOURCE.delete(1);
          B1REFERENCE.delete(1);
          B1ELEMENTX.delete(1);
          B1SOURCEX.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B1(B1RID.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 then
      pselect_B1;
    end if;

  null;
 end;
  procedure pcommit_P is
  begin
    for i__ in 1..PFELEMENTID.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit_B1 is
  begin
    for i__ in 1..B1RS.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if substr(B1RS(i__),1,1) = 'I' then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.


-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

-- Generated UPDATE statement. Use (i__) to access fields values.
if substr(B1RS(i__),1,1) = 'U' then
update RASD_ELEMENTS set
  HIDDENYN = B1HIDDENYN(i__)
 ,SOURCE = B1SOURCE(i__)
where ROWID = B1RID(i__);
 MESSAGE := 'Data is changed.';
end if;


      null; end if;
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 then
       pcommit_B1;
    end if;

  null;
  end;
  procedure formgen_js is
  begin
  --Input parameter in JS: LANG
  --Input parameter in JS: PFORM
  --Input parameter in JS: PFORMID
    htp.p('function js_Slov$elementi(pvalue, pobjectname) {');
      htp.p(' eval(''val_''+pobjectname+''="''+pvalue.replace(/"/g, "&#34;")+''"'');');
      htp.p(' var x = document.getElementById(pobjectname+''_RASD''); ');
    begin
    for r__ in (
--<lovsql formid="102" linkid="lov$elementi">
                  select -1 orderby, '' label, to_number(null) id

                    from dual

                  union

                  select orderby,

                          decode(nameid, null, element, nameid) label,

                          elementid id

                    from RASD_ELEMENTS

                   where formid = PFORMID

                     and (element in ('HTML_', 'HEAD_', 'BODY_') or

                         nameid is not null)

                   order by orderby
--</lovsql>
    ) loop
      htp.p('  var option = document.createElement("option"); option.value="'||replace(replace(r__.id,'''','\'''),'"','\"')||'"; option.text = "'||replace(replace(r__.label,'''','\'''),'"','\"')||'"; option.selected = ((pvalue=='''||replace(replace(r__.id,'''','\'''),'"','\"')||''')?'' selected '':''''); x.append(option);');
    end loop;
    exception when others then
      raise_application_error('-20000','Error in lov$elementi: '||sqlerrm);
    end;
    htp.p('}');
    htp.p('function js_Slov$tclevel(pvalue, pobjectname) {');
      htp.p(' eval(''val_''+pobjectname+''="''+pvalue.replace(/"/g, "&#34;")+''"'');');
      htp.p(' var x = document.getElementById(pobjectname+''_RASD''); ');
    begin
    for r__ in (
--<lovsql formid="102" linkid="lov$tclevel">
                 select 1 vr, '' label, '' id

                    from dual

                  union

                  select 2 vr,

                          RASDI_TRNSLT.text('Form - blocks', lang) label,

                          'FB' id

                    from dual

                  union

                  select 3 vr,

                          RASDI_TRNSLT.text('Blocks - fields', lang) label,

                          'BP' id

                    from dual

                  union

                  select 4 vr, '1 level' label, '1' id

                    from dual

                  union

                  select 5 vr, '2 level' label, '2' id

                    from dual

                  union

                  select 6 vr, '3 level' label, '3' id

                    from dual

                  union

                  select 7 vr, '4 level' label, '4' id

                    from dual

                  union

                  select 8 vr, '5 level' label, '5' id

                    from dual

                  union

                  select 9 vr, '6 level' label, '6' id

                    from dual

                   order by vr
--</lovsql>
    ) loop
      htp.p('  var option = document.createElement("option"); option.value="'||replace(replace(r__.id,'''','\'''),'"','\"')||'"; option.text = "'||replace(replace(r__.label,'''','\'''),'"','\"')||'"; option.selected = ((pvalue=='''||replace(replace(r__.id,'''','\'''),'"','\"')||''')?'' selected '':''''); x.append(option);');
    end loop;
    exception when others then
      raise_application_error('-20000','Error in lov$tclevel: '||sqlerrm);
    end;
    htp.p('}');
    htp.p('function js_Slov$source(pvalue, pobjectname) {');
      htp.p(' eval(''val_''+pobjectname+''="''+pvalue.replace(/"/g, "&#34;")+''"'');');
      htp.p(' var x = document.getElementById(pobjectname+''_RASD''); ');
      htp.p('  var option = document.createElement("option"); option.value=""; option.text = ""; option.selected = ((pvalue=='''')?'' selected '':''''); x.append(option);');
      htp.p('  var option = document.createElement("option"); option.value="G"; option.text = "'|| RASDI_TRNSLT.text('Eng.', lang) ||'"; option.selected = ((pvalue==''G'')?'' selected '':''''); x.append(option);');
      htp.p('  var option = document.createElement("option"); option.value="V"; option.text = "'|| RASDI_TRNSLT.text('Chng.', lang) ||'"; option.selected = ((pvalue==''V'')?'' selected '':''''); x.append(option);');
      htp.p('  var option = document.createElement("option"); option.value="R"; option.text = "'|| RASDI_TRNSLT.text('Ref.', lang) ||'"; option.selected = ((pvalue==''R'')?'' selected '':''''); x.append(option);');
    htp.p('}');
    htp.p('function js_Clink$CHKBXDinit(pvalue, pobjectname) {');
    htp.p('          var element = document.getElementById(pobjectname + ''_RASD'');  ');
    htp.p('          if (element.value == ''Y'') {  element.checked = true;  }  ');
    htp.p('          else { element.checked = false;  } ');
    htp.p('}');
    htp.p('function js_Clink$CHKBXDclick(pvalue, pobjectname) {');
    htp.p('          var element = document.getElementById(pobjectname + ''_RASD'');  ');
    htp.p('          if (element.checked) { element.value=''Y''; } else { element.value=''N'';}  ');
    htp.p('}');
    htp.p('function cMFP() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB1() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
    iB1 pls_integer;
  function ShowFieldCOMPID return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldERROR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONBCK return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCLR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCOMPILE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONDELEL return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONFWD return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONPREV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONRES return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSAVE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSRC return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldHINTCONTENT return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldMESSAGE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldPFORM return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVLOB return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVUSER return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldWARNING return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB1_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then
       return true;
    end if;
    return false;
  end;
  function js_link$attributs(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'B1ATTS%' then
      v_return := v_return || '''!rasdc_attributes.program?PELEMENTID=''+document.RASDC2_HTML.B1ELEMENTID_'||substr(name,instr(name,'_',-1)+1)||'.value+
''&PELEMENT=''+document.RASDC2_HTML.B1ELEMENTX_'||substr(name,instr(name,'_',-1)+1)||'.value+
''&LANG=''+document.RASDC2_HTML.LANG.value+
''&PFORMID=''+document.RASDC2_HTML.PFORMID.value+
''''';
    elsif name is null then
      v_return := v_return ||'''!rasdc_attributes.program?PELEMENTID=''+document.RASDC2_HTML.B1ELEMENTID_1.value+
''&PELEMENT=''+document.RASDC2_HTML.B1ELEMENTX_1.value+
''&LANG=''+document.RASDC2_HTML.LANG.value+
''&PFORMID=''+document.RASDC2_HTML.PFORMID.value+
''''';
    end if;
    return v_return;
  end;
  procedure js_link$attributs(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$attributs(value, name));
  end;
  function js_link$DOWNLOAD(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'PDOWNLOAD%' then
      v_return := v_return || '''RASDC_FILES.download?PDATOTEKA=''+document.RASDC2_HTML.PFORMID.value+
''''';
    elsif name is null then
      v_return := v_return ||'''RASDC_FILES.download?PDATOTEKA=''+document.RASDC2_HTML.PFORMID.value+
''''';
    end if;
    return v_return;
  end;
  procedure js_link$DOWNLOAD(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$DOWNLOAD(value, name));
  end;
  function js_link$upload(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'PUPLOAD%' then
      v_return := v_return || '''RASDC_FILES.upload?PLANG=''+document.RASDC2_HTML.LANG.value+
''&PFORMID=''+document.RASDC2_HTML.PFORMID.value+
''''';
    elsif name is null then
      v_return := v_return ||'''RASDC_FILES.upload?PLANG=''+document.RASDC2_HTML.LANG.value+
''&PFORMID=''+document.RASDC2_HTML.PFORMID.value+
''''';
    end if;
    return v_return;
  end;
  procedure js_link$upload(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$upload(value, name));
  end;
  procedure js_Slov$elementi(value varchar2, name varchar2 default null) is
  begin
    htp.p('<script language="JavaScript">');
    htp.p('js_Slov$elementi('''||replace(replace(value,'''','\'''),'"','\"')||''','''||name||''');');
    htp.p('</script>');
  end;
  procedure js_Slov$tclevel(value varchar2, name varchar2 default null) is
  begin
    htp.p('<script language="JavaScript">');
    htp.p('js_Slov$tclevel('''||replace(replace(value,'''','\'''),'"','\"')||''','''||name||''');');
    htp.p('</script>');
  end;
  procedure js_Slov$source(value varchar2, name varchar2 default null) is
  begin
    htp.p('<script language="JavaScript">');
    htp.p('js_Slov$source('''||replace(replace(value,'''','\'''),'"','\"')||''','''||name||''')');
    htp.p('</script>');
  end;
procedure output_B1_DIV is begin htp.p('');  if  ShowBlockB1_DIV  then
htp.prn('<div  id="B1_DIV" class="rasdblock"><div>
<caption><div id="B1_LAB" class="labelblock"></div></caption><table border="1" id="B1_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB1" id="rasdTxLabB1ATTS"><span id="B1ATTS_LAB" class="label"></span></td><td class="rasdTxLab rasdTxLabBlockB1" id="rasdTxLabB1ELEMENT"><span id="B1ELEMENT_LAB" class="label">'|| showLabel('Element','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB1" id="rasdTxLabB1HIDDENYN"><span id="B1HIDDENYN_LAB" class="label">'|| showLabel('Hidden','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB1" id="rasdTxLabB1SOURCE"><span id="B1SOURCE_LAB" class="label">'|| showLabel('Source','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB1" id="rasdTxLabB1REFERENCE"><span id="B1REFERENCE_LAB" class="label">'|| showLabel('Ref. to id','',0) ||'</span></td></tr></thead>'); for iB1 in 1..B1RID.count loop
htp.prn('<tr id="B1_BLOCK_'||iB1||'"><span id="" value="'||iB1||'" name="" class="hiddenRowItems"><input name="B1RS_'||iB1||'" id="B1RS_'||iB1||'_RASD" type="hidden" value="'||B1RS(iB1)||'"/>
<input name="B1RID_'||iB1||'" id="B1RID_'||iB1||'_RASD" type="hidden" value="'||to_char(B1RID(iB1))||'"/>
<input name="B1ELEMENTID_'||iB1||'" id="B1ELEMENTID_'||iB1||'_RASD" type="hidden" value="'||ltrim(to_char(B1ELEMENTID(iB1)))||'"/>
<input name="B1ELEMENTX_'||iB1||'" id="B1ELEMENTX_'||iB1||'_RASD" type="hidden" value="'||B1ELEMENTX(iB1)||'"/>
</span><td class="rasdTxB1ATTS rasdTxTypeC" id="rasdTxB1ATTS_'||iB1||'">');  if B1ATTS#SET(iB1).visible then
htp.prn('<a href="javascript: var link=window.open(encodeURI('); js_link$attributs(B1ATTS(iB1),'B1ATTS_'||iB1||'');
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" name="B1ATTS_'||iB1||'" id="B1ATTS_'||iB1||'_RASD" ');  if  B1ATTS(iB1) is not null and ''|| RASDI_TRNSLT.text('Attributes of element',LANG) ||'' is not null  then htp.prn('title="'|| RASDI_TRNSLT.text('Attributes of element',LANG) ||'"'); end if;
htp.prn('');  if B1ATTS#SET(iB1).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B1ATTS#SET(iB1).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B1ATTS#SET(iB1).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B1ATTS#SET(iB1).custom);
htp.prn('');  if B1ATTS#SET(iB1).error is not null then htp.p(' title="'||B1ATTS#SET(iB1).error||'"'); end if;
htp.prn('');  if B1ATTS#SET(iB1).info is not null then htp.p(' title="'||B1ATTS#SET(iB1).info||'"'); end if;
htp.prn('>'||B1ATTS(iB1)||'</a>');  end if;
htp.prn('</td><td class="rasdTxB1ELEMENT rasdTxTypeC" id="rasdTxB1ELEMENT_'||iB1||'"><font id="B1ELEMENT_'||iB1||'_RASD" class="rasdFont">'||B1ELEMENT(iB1)||'</font></td><td class="rasdTxB1HIDDENYN rasdTxTypeC" id="rasdTxB1HIDDENYN_'||iB1||'"><input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B1HIDDENYN_'||iB1||''');" name="B1HIDDENYN_'||iB1||'" id="B1HIDDENYN_'||iB1||'_RASD" type="checkbox" value="'||B1HIDDENYN(iB1)||'" class="rasdCheckbox "/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B1HIDDENYN(iB1)||''' , ''B1HIDDENYN_'||iB1||''' ); }) </SCRIPT>
</td><td class="rasdTxB1SOURCE rasdTxTypeC" id="rasdTxB1SOURCE_'||iB1||'"><select name="B1SOURCE_'||iB1||'" ID="B1SOURCE_'||iB1||'_RASD" class="rasdSelect ">'); js_Slov$source(B1SOURCE(iB1),'B1SOURCE_'||iB1||'');
htp.prn('</select></td><td class="rasdTxB1REFERENCE rasdTxTypeC" id="rasdTxB1REFERENCE_'||iB1||'"><font id="B1REFERENCE_'||iB1||'_RASD" class="rasdFont">'||B1REFERENCE(iB1)||'</font></td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
procedure output_P_DIV is begin htp.p('');  if  ShowBlockP_DIV  then
htp.prn('<div  id="P_DIV" class="rasdblock"><div>
<caption><div id="P_LAB" class="labelblock"></div></caption>
<table border="0" id="P_TABLE"><thead><tr><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPFELEMENTID"><span id="PFELEMENTID_LAB" class="label"></span></td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPTCLEVEL"><span id="PTCLEVEL_LAB" class="label"></span></td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPGBTNSRC"><span id="PGBTNSRC_LAB" class="label"></span></td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPUPLOAD"><span id="PUPLOAD_LAB" class="label"></span></td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPDOWNLOAD"><span id="PDOWNLOAD_LAB" class="label"></span></td></tr></thead><tr id="P_BLOCK_1"><td class="rasdTxPFELEMENTID rasdTxTypeC" id="rasdTxPFELEMENTID_1"><select name="PFELEMENTID_1" ID="PFELEMENTID_1_RASD" class="rasdSelect ">'); js_Slov$elementi(PFELEMENTID(1),'PFELEMENTID_1');
htp.prn('</select></td><td class="rasdTxPTCLEVEL rasdTxTypeC" id="rasdTxPTCLEVEL_1"><select name="PTCLEVEL_1" ID="PTCLEVEL_1_RASD" class="rasdSelect ">'); js_Slov$tclevel(PTCLEVEL(1),'PTCLEVEL_1');
htp.prn('</select></td><td class="rasdTxPGBTNSRC rasdTxTypeC" id="rasdTxPGBTNSRC_1"><input onclick=" ACTION.value=this.value; submit();" name="PGBTNSRC_1" id="PGBTNSRC_1_RASD" type="button" value="'||PGBTNSRC(1)||'" class="rasdButton"/>
</td><td class="rasdTxPUPLOAD rasdTxTypeC" id="rasdTxPUPLOAD_1"><IMG ONCLICK="javascript: var link=window.open(encodeURI('); js_link$upload(PUPLOAD(1),'PUPLOAD_1');
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" ');  if  PUPLOAD(1) is not null and ''|| RASDI_TRNSLT.text('Upload',LANG) ||'' is not null  then htp.prn('title="'|| RASDI_TRNSLT.text('Upload',LANG) ||'"'); end if;
htp.prn(' ID="PUPLOAD_1_RASD" CLASS="rasdImg" SRC="'||PUPLOAD(1)||'"></IMG></td><td class="rasdTxPDOWNLOAD rasdTxTypeC" id="rasdTxPDOWNLOAD_1"><IMG ONCLICK="javascript: var link=window.open(encodeURI('); js_link$DOWNLOAD(PDOWNLOAD(1),'PDOWNLOAD_1');
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" ');  if  PDOWNLOAD(1) is not null and ''|| RASDI_TRNSLT.text('Download',LANG) ||'' is not null  then htp.prn('title="'|| RASDI_TRNSLT.text('Download',LANG) ||'"'); end if;
htp.prn(' ID="PDOWNLOAD_1_RASD" CLASS="rasdImg" SRC="'||PDOWNLOAD(1)||'"></IMG></td></tr></table></div></div>');  end if;
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');

htp.prn('</head>
<body><div id="RASDC2_HTML_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_HTML_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div id="RASDC2_HTML_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASDC2_HTML_MENU') ||'     </div>
<form name="RASDC2_HTML" method="post" action="?"><div id="RASDC2_HTML_DIV" class="rasdForm"><div id="RASDC2_HTML_HEAD" class="rasdFormHead"><input name="RECNUMP" id="RECNUMP_RASD" type="hidden" value="'||ltrim(to_char(RECNUMP))||'"/>
<input name="RECNUMB10" id="RECNUMB10_RASD" type="hidden" value="'||RECNUMB10||'"/>
<input name="RECNUMB1" id="RECNUMB1_RASD" type="hidden" value="'||ltrim(to_char(RECNUMB1))||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
');
if  ShowFieldGBUTTONBCK  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONBCK" id="GBUTTONBCK_RASD" type="button" value="'||GBUTTONBCK||'" class="rasdButton"/>');  end if;
htp.prn('
');
if  ShowFieldGBUTTONFWD  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONFWD" id="GBUTTONFWD_RASD" type="button" value="'||GBUTTONFWD||'" class="rasdButton"/>');  end if;
htp.prn('
<input name="LANG" id="LANG_RASD" type="hidden" value="'||LANG||'"/>
<input name="PFORMID" id="PFORMID_RASD" type="hidden" value="'||PFORMID||'"/>
');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldGBUTTONDELEL  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONDELEL" id="GBUTTONDELEL_RASD" type="button" value="'||GBUTTONDELEL||'" class="rasdButton"/>');  end if;
htp.prn('
<input name="POLJE" id="POLJE_RASD" type="hidden" value="'||POLJE||'"/>
');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div><div id="RASDC2_HTML_RESPONSE" class="rasdFormResponse"><div id="RASDC2_HTML_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASDC2_HTML_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASDC2_HTML_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div></div><div id="RASDC2_HTML_BODY" class="rasdFormBody">'); output_P_DIV; htp.p(''); output_B1_DIV; htp.p('</div><div id="RASDC2_HTML_FOOTER" class="rasdFormFooter">');
if  ShowFieldGBUTTONBCK  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONBCK" id="GBUTTONBCK_RASD" type="button" value="'||GBUTTONBCK||'" class="rasdButton"/>');  end if;
htp.prn('
');
if  ShowFieldGBUTTONFWD  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONFWD" id="GBUTTONFWD_RASD" type="button" value="'||GBUTTONFWD||'" class="rasdButton"/>');  end if;
htp.prn('
');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('
');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldGBUTTONDELEL  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONDELEL" id="GBUTTONDELEL_RASD" type="button" value="'||GBUTTONDELEL||'" class="rasdButton"/>');  end if;
htp.prn('
');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div></div></form><div id="RASDC2_HTML_BOTTOM" class="rasdFormBottom">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_HTML_BOTTOM',1,instr('RASDC2_HTML_BOTTOM', '_',-1)-1) , '') ||'</div></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB1_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then
       return true;
    end if;
    return false;
  end;
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>');
    htpp('<form name="RASDC2_HTML" version="'||version||'">');
    htpp('<formfields>');
    htpp('<recnumb1>'||RECNUMB1||'</recnumb1>');
    htpp('<recnumb10><![CDATA['||RECNUMB10||']]></recnumb10>');
    htpp('<recnump>'||RECNUMP||'</recnump>');
    htpp('<action><![CDATA['||ACTION||']]></action>');
    htpp('<page>'||PAGE||'</page>');
    htpp('<gbuttonbck><![CDATA['||GBUTTONBCK||']]></gbuttonbck>');
    htpp('<gbuttonfwd><![CDATA['||GBUTTONFWD||']]></gbuttonfwd>');
    htpp('<lang><![CDATA['||LANG||']]></lang>');
    htpp('<pformid><![CDATA['||PFORMID||']]></pformid>');
    htpp('<gbuttonsave><![CDATA['||GBUTTONSAVE||']]></gbuttonsave>');
    htpp('<gbuttoncompile><![CDATA['||GBUTTONCOMPILE||']]></gbuttoncompile>');
    htpp('<gbuttonres><![CDATA['||GBUTTONRES||']]></gbuttonres>');
    htpp('<gbuttonprev><![CDATA['||GBUTTONPREV||']]></gbuttonprev>');
    htpp('<gbuttondelel><![CDATA['||GBUTTONDELEL||']]></gbuttondelel>');
    htpp('<error><![CDATA['||ERROR||']]></error>');
    htpp('<message><![CDATA['||MESSAGE||']]></message>');
    htpp('<warning><![CDATA['||WARNING||']]></warning>');
    htpp('<hintcontent><![CDATA['||HINTCONTENT||']]></hintcontent>');
    htpp('<polje><![CDATA['||POLJE||']]></polje>');
    htpp('</formfields>');
    if ShowBlockp_DIV then
    htpp('<p>');
    htpp('<element>');
    htpp('<pfelementid><![CDATA['||PFELEMENTID(1)||']]></pfelementid>');
    htpp('<ptclevel><![CDATA['||PTCLEVEL(1)||']]></ptclevel>');
    htpp('<pgbtnsrc><![CDATA['||PGBTNSRC(1)||']]></pgbtnsrc>');
    htpp('<pupload><![CDATA['||PUPLOAD(1)||']]></pupload>');
    htpp('<pdownload><![CDATA['||PDOWNLOAD(1)||']]></pdownload>');
    htpp('</element>');
  htpp('</p>');
  end if;
    if ShowBlockb1_DIV then
    htpp('<b1>');
  for i__ in 1..
B1RID
.count loop
    htpp('<element>');
    htpp('<b1rs><![CDATA['||B1RS(i__)||']]></b1rs>');
    htpp('<b1rid>'||B1RID(i__)||'</b1rid>');
    htpp('<b1elementid>'||B1ELEMENTID(i__)||'</b1elementid>');
    htpp('<b1atts><![CDATA['||B1ATTS(i__)||']]></b1atts>');
    htpp('<b1element><![CDATA['||B1ELEMENT(i__)||']]></b1element>');
    htpp('<b1hiddenyn><![CDATA['||B1HIDDENYN(i__)||']]></b1hiddenyn>');
    htpp('<b1source><![CDATA['||B1SOURCE(i__)||']]></b1source>');
    htpp('<b1reference><![CDATA['||B1REFERENCE(i__)||']]></b1reference>');
    htpp('<b1elementx><![CDATA['||B1ELEMENTX(i__)||']]></b1elementx>');
    htpp('</element>');
  end loop;
  htpp('</b1>');
  end if;
    htpp('</form>');
else
    htpp('{"form":{"@name":"RASDC2_HTML","@version":"'||version||'",' );
    htpp('"formfields": {');
    htpp('"recnumb1":"'||RECNUMB1||'"');
    htpp(',"recnumb10":"'||escapeRest(RECNUMB10)||'"');
    htpp(',"recnump":"'||RECNUMP||'"');
    htpp(',"action":"'||escapeRest(ACTION)||'"');
    htpp(',"page":"'||PAGE||'"');
    htpp(',"gbuttonbck":"'||escapeRest(GBUTTONBCK)||'"');
    htpp(',"gbuttonfwd":"'||escapeRest(GBUTTONFWD)||'"');
    htpp(',"lang":"'||escapeRest(LANG)||'"');
    htpp(',"pformid":"'||escapeRest(PFORMID)||'"');
    htpp(',"gbuttonsave":"'||escapeRest(GBUTTONSAVE)||'"');
    htpp(',"gbuttoncompile":"'||escapeRest(GBUTTONCOMPILE)||'"');
    htpp(',"gbuttonres":"'||escapeRest(GBUTTONRES)||'"');
    htpp(',"gbuttonprev":"'||escapeRest(GBUTTONPREV)||'"');
    htpp(',"gbuttondelel":"'||escapeRest(GBUTTONDELEL)||'"');
    htpp(',"error":"'||escapeRest(ERROR)||'"');
    htpp(',"message":"'||escapeRest(MESSAGE)||'"');
    htpp(',"warning":"'||escapeRest(WARNING)||'"');
    htpp(',"hintcontent":"'||escapeRest(HINTCONTENT)||'"');
    htpp(',"polje":"'||escapeRest(POLJE)||'"');
    htpp('},');
    if ShowBlockp_DIV then
    htpp('"p":[');
     htpp('{');
    htpp('"pfelementid":"'||escapeRest(PFELEMENTID(1))||'"');
    htpp(',"ptclevel":"'||escapeRest(PTCLEVEL(1))||'"');
    htpp(',"pgbtnsrc":"'||escapeRest(PGBTNSRC(1))||'"');
    htpp(',"pupload":"'||escapeRest(PUPLOAD(1))||'"');
    htpp(',"pdownload":"'||escapeRest(PDOWNLOAD(1))||'"');
    htpp('}');
    htpp(']');
  else
    htpp('"p":[]');
  end if;
    if ShowBlockb1_DIV then
    htpp(',"b1":[');
  v_firstrow__ := true;
  for i__ in 1..
B1RID
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b1rs":"'||escapeRest(B1RS(i__))||'"');
    htpp(',"b1rid":"'||B1RID(i__)||'"');
    htpp(',"b1elementid":"'||B1ELEMENTID(i__)||'"');
    htpp(',"b1atts":"'||escapeRest(B1ATTS(i__))||'"');
    htpp(',"b1element":"'||escapeRest(B1ELEMENT(i__))||'"');
    htpp(',"b1hiddenyn":"'||escapeRest(B1HIDDENYN(i__))||'"');
    htpp(',"b1source":"'||escapeRest(B1SOURCE(i__))||'"');
    htpp(',"b1reference":"'||escapeRest(B1REFERENCE(i__))||'"');
    htpp(',"b1elementx":"'||escapeRest(B1ELEMENTX(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp(',"b1":[]');
  end if;
    htpp('}}');
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

--<ON_ACTION formid="102" blockid="">
  rasdc_library.log(this_form,pformid, 'START', compid);



  psubmit(name_array ,value_array);

  rasd_client.secCheckPermission(this_form,ACTION);



  RASDC_LIBRARY.checkprivileges(PFORMID);



 if ACTION is null then null;

    RECNUMP := 1;

    RECNUMB1 := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONBCK then     if RECNUMP > 1 then

      RECNUMP := RECNUMP-1;

    else

      RECNUMP := 1;

    end if;

    if RECNUMB1 > 50 then

      RECNUMB1 := RECNUMB1-50;

    else

      RECNUMB1 := 1;

    end if;

    pselect;

    message := 'Page '||to_char( ((RECNUMB1 - mod(RECNUMB1,50))/50)+1);



    poutput;

  elsif ACTION = GBUTTONFWD then     RECNUMP := RECNUMP+1;

    RECNUMB1 := RECNUMB1+50;

    pselect;

    message := 'Page '||to_char( ((RECNUMB1 - mod(RECNUMB1,50))/50)+1);



    poutput;

  elsif ACTION = GBUTTONSRC then     RECNUMP := 1;

    RECNUMB1 := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONDELEL then

        delete from rasd_elements t where t.formid = pformid and not exists (select 1 from rasd_attributes a where a.formid = pformid and a.elementid = t.elementid and a.source in ('V','R')) and nvl(t.source,'G') not in  ('V','R');

        delete from rasd_attributes t where formid = pformid and not exists (select 1 from rasd_elements a where a.formid = pformid and a.elementid = t.elementid);

        update rasd_elements set pelementid = 0, elementid=decode(substr(to_char(elementid),1,1),'-',elementid,elementid*-1) where formid = pformid;

        update rasd_attributes set elementid=decode(substr(to_char(elementid),1,1),'-',elementid,elementid*-1) where formid = pformid;



        message := RASDI_TRNSLT.text('All HTML elements are deleted. Except manualy changed.', lang);

          pselect;

        poutput;

  elsif ACTION = GBUTTONSAVE then

        pcommit;



        pselect;

        message := RASDI_TRNSLT.text('Changes are saved.', lang);



		for r in (select 1 from rasd_forms f where f.formid = PFORMID and f.autodeletehtmlyn = 'Y') loop

		WARNING :=  RASDI_TRNSLT.text('Info: AutoDELETE HTML on form is turnd ON.', lang);

		end loop;



	    poutput;

  elsif ACTION = GBUTTONCLR then     pclear;

    poutput;



  elsif ACTION = GBUTTONCOMPILE then



        rasdc_library.log(this_form,pformid, 'COMMIT_S', compid);

        pcommit;

        rasdc_library.log(this_form,pformid, 'COMMIT_E', compid);

        commit;

        rasdc_library.log(this_form,pformid, 'REF_S', compid);

        rasdc_library.RefData(PFORMID);

        rasdc_library.log(this_form,pformid, 'REF_E', compid);



        compile(pformid , pform , lang ,  message, ''  , compid);



        rasdc_library.log(this_form,pformid, 'SELECT_S', compid);

        pselect;

        rasdc_library.log(this_form,pformid, 'SELECT_E', compid);



      rasdc_library.log(this_form,pformid, 'POUTPUT_S', compid);



		for r in (select 1 from rasd_forms f where f.formid = PFORMID and f.autodeletehtmlyn = 'Y') loop

		WARNING :=  RASDI_TRNSLT.text('Info: AutoDELETE HTML on form is turnd ON.', lang);

		end loop;



    poutput;

      rasdc_library.log(this_form,pformid, 'POUTPUT_E', compid);



  end if;

 rasdc_library.log(this_form,pformid, 'END', compid);
--</ON_ACTION>
--<POST_ACTION formid="102" blockid="">
null;
--</POST_ACTION>
    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head><body><div id="RASDC2_HTML_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_HTML_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASDC2_HTML' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_HTML_FOOTER',1,instr('RASDC2_HTML_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end;
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_HTML',ACTION);
  if ACTION = GBUTTONSAVE then     pselect;
    pcommit;
  end if;

--<POST_ACTION formid="102" blockid="">
null;
--</POST_ACTION>
-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end;
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_HTML',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB1 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONBCK then     if RECNUMP > 1 then
      RECNUMP := RECNUMP-1;
    else
      RECNUMP := 1;
    end if;
    if RECNUMB1 > 50 then
      RECNUMB1 := RECNUMB1-50;
    else
      RECNUMB1 := 1;
    end if;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONFWD then     RECNUMP := RECNUMP+1;
    RECNUMB1 := RECNUMB1+50;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     RECNUMP := 1;
    RECNUMB1 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutputrest;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutputrest;
  end if;

--<POST_ACTION formid="102" blockid="">
null;
--</POST_ACTION>
-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="RASDC2_HTML" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('{"form":{"@name":"RASDC2_HTML","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end;
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>102</formid><form>RASDC2_HTML</form><version>1</version><change>28.02.2024 09/09/34</change><user>RASDDEV</user><label><![CDATA[<%= rasdc_library.formName(PFORMID, LANG) %>]]></label><lobid>RASDDEV</lobid><program>?</program><referenceyn>Y</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>11.09.2023 02/02/39</change><compileyn>Y</compileyn><application>RASD 2.0</application><owner>domen</owner><editor>domen</editor></info></compiledInfo><blocks><block><blockid>B1</blockid><sqltable>RASD_ELEMENTS</sqltable><numrows>50</numrows><emptyrows>-50</emptyrows><dbblockyn>Y</dbblockyn><rowidyn>Y</rowidyn><pagingyn>Y</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[select rowid rid,

                 elementid,

				 ''*'' atts,

                 ''<font size="-3">'' ||

                  substr(''1---2---3---4---5---6---7---8---9--10'' ||

                         ''--11--12--13--14--15--16--17--18--19--20'' ||

                         ''--21--22--23--24--25--26--27--28--29--30'',

                         1,

                         ((level - 1) * 4 + 1)) || ''</font>   <B>'' ||

                  element || ''  '' ||

                  decode(nvl(nameid, id), null, '''', '' <font color=blue>'') ||

                  nvl(nameid, id) ||

                  decode(nvl(nameid, id), null, '''', ''</font>'') || ''</B>'' || ''('' ||

                  elementid || '') '' element,

                 hiddenyn,

                 nvl(source, ''?''),

                 rform || ''.'' || rid referenca,

                 element || ''('' || nameid || '')'' elementx,

                 decode( (select count(*) from rasd_attributes u where u.formid=r.formid and u.elementid=r.elementid and nvl(u.source,''G'') <> ''G''), 0 , '''' , ''X''  )||

				 decode(nvl(r.source,''G''),''G'','''',''X'') sourcex

            from RASD_ELEMENTS r

           where formid = PFORMID

             and ((ptclevel = ''FB'' and element in (''FORM_'', ''TR_'', ''P_'') and

                 nameid is not null) or

                 (ptclevel = ''BP'' and

                 ((element in (''TR_'', ''P_'') and nameid is not null) or

                 (ele';
 v_vc(2) := 'ment not in (''TR_'', ''P_'') and nameid is not null))) or

                 (ptclevel = ''1'' and level <= 2) or

                 (ptclevel = ''2'' and level <= 3) or

                 (ptclevel = ''3'' and level <= 4) or

                 (ptclevel = ''4'' and level <= 5) or ptclevel is null)

          connect by formid = prior formid

                 and pelementid = prior elementid

           start with formid = PFORMID

                  and ((pfelementid is null and pelementid = 0) or

                      (elementid = pfelementid))

           order siblings by orderby]]></sqltext><label></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B1</blockid><fieldid>ATTS</fieldid><type>C</type><format></format><element>A_</element><hiddenyn></hiddenyn><orderby>1002</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1ATTS</nameid><label></label><linkid>link$attributs</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B1</blockid><fieldid>ELEMENT</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>1003</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1ELEMENT</nameid><label><![CDATA[<%= showLabel(''Element'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>ELEMENTID</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>1001</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1ELEMENTID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><inc';
 v_vc(3) := 'ludevis>N</includevis></field><field><blockid>B1</blockid><fieldid>ELEMENTX</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>1040</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1ELEMENTX</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>HIDDENYN</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1010</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>Y</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1HIDDENYN</nameid><label><![CDATA[<%= showLabel(''Hidden'','''',0) %>]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>REFERENCE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>1030</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1REFERENCE</nameid><label><![CDATA[<%= showLabel(''Ref. to id'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>RID</fieldid><type>R</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>999</orderby><pkyn>Y</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1RID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blo';
 v_vc(4) := 'ckid><fieldid>RS</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>998</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1RS</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>SOURCE</fieldid><type>C</type><format></format><element>SELECT_</element><hiddenyn></hiddenyn><orderby>1020</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>Y</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1SOURCE</nameid><label><![CDATA[<%= showLabel(''Source'','''',0) %>]]></label><linkid>lov$source</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>SOURCEX</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B1SOURCEX</nameid><label><![CDATA[sourcex]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>P</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>Y</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>P</blockid><fieldid>DOWNLOAD</fieldid><type>C</type><format></format><element>IMG_</element><hiddenyn></hiddenyn><orderby>150</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''rasdc_files.sh';
 v_vc(5) := 'owfile?pfile=pict/gumbhtmld.jpg'']]></defaultval><elementyn>Y</elementyn><nameid>PDOWNLOAD</nameid><label></label><linkid>link$DOWNLOAD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>FELEMENTID</fieldid><type>C</type><format></format><element>SELECT_</element><hiddenyn></hiddenyn><orderby>90</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFELEMENTID</nameid><label></label><linkid>lov$elementi</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>FILTER</fieldid><type>C</type><format></format><element>INPUT_TEXT</element><hiddenyn></hiddenyn><orderby>109</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PFILTER</nameid><label><![CDATA[<%= showLabel(''Filter'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>GBTNSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>120</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[gbuttonsrc]]></defaultval><elementyn>Y</elementyn><nameid>PGBTNSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>PF</fieldid><type>C</type><format></format><element>INPUT_TEXT</element><hiddenyn></hiddenyn><orderby>110</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>';
 v_vc(6) := 'PF</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>P</blockid><fieldid>TCLEVEL</fieldid><type>C</type><format></format><element>SELECT_</element><hiddenyn></hiddenyn><orderby>105</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PTCLEVEL</nameid><label></label><linkid>lov$tclevel</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>UPLOAD</fieldid><type>C</type><format></format><element>IMG_</element><hiddenyn></hiddenyn><orderby>140</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''rasdc_files.showfile?pfile=pict/gumbhtmlu.jpg'']]></defaultval><elementyn>Y</elementyn><nameid>PUPLOAD</nameid><label></label><linkid>link$upload</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ACTION</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>COMPID</fieldid><type>N</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>COMPID</nameid><label></label><linkid></linkid><source>';
 v_vc(7) := 'V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ERROR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONBCK</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>2</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''<<'']]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONBCK</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCLR</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONCLR'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONCLR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCLR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>13</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Compile'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONCOMPILE</nameid><label></label><linkid></linkid><source>V</source><rlob';
 v_vc(8) := 'id>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCOMPILE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONDELEL</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>17</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Delete all HTML elements'', lang)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONDELEL</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONFWD</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>3</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''>>'']]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONFWD</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONPREV</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>15</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Preview'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONPREV</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONPREV</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONRES</fieldid><type>C</type><format></format><element>INPUT_RESET</element><hiddenyn></hiddenyn><orderby>14</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Reset'',LANG)]]></defaultval><elementyn>Y</elemen';
 v_vc(9) := 'tyn><nameid>GBUTTONRES</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONRES</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>12</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Save'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONSAVE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSAVE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>9</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Search'',LANG)]]></defaultval><elementyn>N</elementyn><nameid>GBUTTONSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSRC</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>HINTCONTENT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>HINTCONTENT</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>HINTCONTENT</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>LANG</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>5</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><';
 v_vc(10) := 'lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>LANG</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>LANG</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>51</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>MESSAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PAGE</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[0]]></defaultval><elementyn>Y</elementyn><nameid>PAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>7</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PFORM</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORM</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORMID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>6</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFORMID</nam';
 v_vc(11) := 'eid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORMID</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>POLJE</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>POLJE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB1</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMB1</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB10</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>RECNUMB10</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMP</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMP</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfi';
 v_vc(12) := 'eldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VLOB</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>81</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VLOB</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VLOB</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VUSER</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>80</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VUSER</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VUSER</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>52</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>WARNING</rfieldid><includevis>N</includevis></field></fields><links><link><linkid>link$CHKBXD</linkid><link>CHKBXD</link><type>C</type><location></location><text></text><source>G</source><hiddenyn>N</hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>FALSE</paramid><type>FALSE</type><orderby>2</orderby><blockid></blockid><fieldid>THIS</fieldid><namecid></namecid><code>N</code><value><![CDATA[N]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TRUE</paramid><type>TRUE</type><orderby>1</orderby><blockid></blockid><fieldid>THIS</fieldid><namecid></namecid><code>Y</code><value><![CDATA[Y]]></value><rlobid></rlobid><rf';
 v_vc(13) := 'orm></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$DOWNLOAD</linkid><link>Download</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[RASDC_FILES.download]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>PFORMID3</paramid><type>OUT</type><orderby>3</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PDATOTEKA</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$attributs</linkid><link>Attributes of element</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!rasdc_attributes.program]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>B1ELEMENTID12</paramid><type>OUT</type><orderby>12</orderby><blockid>B1</blockid><fieldid>ELEMENTID</fieldid><namecid>PELEMENTID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>B1ELEMENTX13</paramid><type>OUT</type><orderby>13</orderby><blockid>B1</blockid><fieldid>ELEMENTX</fieldid><namecid>PELEMENT</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>LANG14</paramid><type>OUT</type><orderby>14</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID15</paramid><type>OUT</type><orderby>15</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$upload</linkid><link>Upload</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[RASDC_FILES.upload]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>LANG18</paramid><type>OUT</type><orderby>18</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>PLANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID19</paramid><type>OUT</type><orderby>19</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlo';
 v_vc(14) := 'bid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>lov$elementi</linkid><link>Elements</link><type>S</type><location></location><text><![CDATA[                  select -1 orderby, '''' label, to_number(null) id

                    from dual

                  union

                  select orderby,

                          decode(nameid, null, element, nameid) label,

                          elementid id

                    from RASD_ELEMENTS

                   where formid = PFORMID

                     and (element in (''HTML_'', ''HEAD_'', ''BODY_'') or

                         nameid is not null)

                   order by orderby]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>lov$source</linkid><link>Source</link><type>T</type><location></location><text></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>TEXT0</paramid><type>TEXT</type><orderby>0</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TEXT1</paramid><type>TEXT</type><orderby>1</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code>G</code><value><![CDATA[<%= RASDI_TRNSLT.text(''Eng.'', lang) %>]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TEXT2</paramid><type>TEXT</type><orderby>2</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code>V</code><value><![CDATA[<%= RASDI_TRNSLT.text(''Chng.'', lang) %>]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TEXT3</paramid><type>TEXT</type><orderby>3</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code>R</code><value><![CDATA[<%= RASDI_TRNSLT.text(''Ref.'', lang) %>]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>lov$tclevel</linkid><link>LEVEL</link><type>S</type><location></location><text><![CDATA[                 select 1 vr, '''' label, '''' id

                    from dual

                  union

                  select 2 vr,

                          RASDI_TRNSLT.text(''Form - blocks'', lang) label,

                          ''FB'' id

                    from dual

  ';
 v_vc(15) := '                union

                  select 3 vr,

                          RASDI_TRNSLT.text(''Blocks - fields'', lang) label,

                          ''BP'' id

                    from dual

                  union

                  select 4 vr, ''1 level'' label, ''1'' id

                    from dual

                  union

                  select 5 vr, ''2 level'' label, ''2'' id

                    from dual

                  union

                  select 6 vr, ''3 level'' label, ''3'' id

                    from dual

                  union

                  select 7 vr, ''4 level'' label, ''4'' id

                    from dual

                  union

                  select 8 vr, ''5 level'' label, ''5'' id

                    from dual

                  union

                  select 9 vr, ''6 level'' label, ''6'' id

                    from dual

                   order by vr]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link></links><pages><pagedata><page>0</page><blockid></blockid><fieldid>PF</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>99</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></page';
 v_vc(16) := 'data><pagedata><page>1</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blocki';
 v_vc(17) := 'd></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>XRFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>XRFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata></pages><triggers><trigger><blockid></blockid><triggerid>FORM_JS</triggerid><plsql><![CDATA[$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

//   setShowHideDiv("B30_DIV", true);

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)





ShowLOVIcon();



HighLightRow(''showrow'', ''lightcoral'')



 });


]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS_REF(82)</triggerid><plsql><![CDATA[$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("<%= RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) %>");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
]]></plsql><plsq';
 v_vc(18) := 'lspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>ON_ACTION</triggerid><plsql><![CDATA[  rasdc_library.log(this_form,pformid, ''START'', compid);



  psubmit(name_array ,value_array);

  rasd_client.secCheckPermission(this_form,ACTION);



  RASDC_LIBRARY.checkprivileges(PFORMID);



 if ACTION is null then null;

    RECNUMP := 1;

    RECNUMB1 := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONBCK then     if RECNUMP > 1 then

      RECNUMP := RECNUMP-1;

    else

      RECNUMP := 1;

    end if;

    if RECNUMB1 > 50 then

      RECNUMB1 := RECNUMB1-50;

    else

      RECNUMB1 := 1;

    end if;

    pselect;

    message := ''Page ''||to_char( ((RECNUMB1 - mod(RECNUMB1,50))/50)+1);



    poutput;

  elsif ACTION = GBUTTONFWD then     RECNUMP := RECNUMP+1;

    RECNUMB1 := RECNUMB1+50;

    pselect;

    message := ''Page ''||to_char( ((RECNUMB1 - mod(RECNUMB1,50))/50)+1);



    poutput;

  elsif ACTION = GBUTTONSRC then     RECNUMP := 1;

    RECNUMB1 := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONDELEL then

        delete from rasd_elements t where t.formid = pformid and not exists (select 1 from rasd_attributes a where a.formid = pformid and a.elementid = t.elementid and a.source in (''V'',''R'')) and nvl(t.source,''G'') not in  (''V'',''R'');

        delete from rasd_attributes t where formid = pformid and not exists (select 1 from rasd_elements a where a.formid = pformid and a.elementid = t.elementid);

        update rasd_elements set pelementid = 0, elementid=decode(substr(to_char(elementid),1,1),''-'',elementid,elementid*-1) where formid = pformid;

        update rasd_attributes set elementid=decode(substr(to_char(elementid),1,1),''-'',elementid,elementid*-1) where formid = pformid;



        message := RASDI_TRNSLT.text(''All HTML elements are deleted. Except manualy changed.'', lang);

          pselect;

        poutput;

  elsif ACTION = GBUTTONSAVE then

        pcommit;



        pselect;

        message := RASDI_TRNSLT.text(''Changes are saved.'', lang);



		for r in (select 1 from rasd_forms f where f.formid = PFORMID and f.autodeletehtmlyn = ''Y'') loop

		WARNING :=  RASDI_TRNSLT.text(''Info: AutoDELETE HTML on form is turnd ON.'', lang);

		end loop;

        	';
 v_vc(19) := '

	    poutput;

  elsif ACTION = GBUTTONCLR then     pclear;

    poutput;



  elsif ACTION = GBUTTONCOMPILE then



        rasdc_library.log(this_form,pformid, ''COMMIT_S'', compid);

        pcommit;

        rasdc_library.log(this_form,pformid, ''COMMIT_E'', compid);

        commit;

        rasdc_library.log(this_form,pformid, ''REF_S'', compid);

        rasdc_library.RefData(PFORMID);

        rasdc_library.log(this_form,pformid, ''REF_E'', compid);



        compile(pformid , pform , lang ,  message, ''''  , compid);



        rasdc_library.log(this_form,pformid, ''SELECT_S'', compid);

        pselect;

        rasdc_library.log(this_form,pformid, ''SELECT_E'', compid);



      rasdc_library.log(this_form,pformid, ''POUTPUT_S'', compid);



		for r in (select 1 from rasd_forms f where f.formid = PFORMID and f.autodeletehtmlyn = ''Y'') loop

		WARNING :=  RASDI_TRNSLT.text(''Info: AutoDELETE HTML on form is turnd ON.'', lang);

		end loop;



    poutput;

      rasdc_library.log(this_form,pformid, ''POUTPUT_E'', compid);



  end if;

 rasdc_library.log(this_form,pformid, ''END'', compid);
]]></plsql><plsqlspec><![CDATA[  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission(''RASDC2_HTML'',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB1 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONBCK then     if RECNUMP > 1 then
      RECNUMP := RECNUMP-1;
    else
      RECNUMP := 1;
    end if;
    if RECNUMB1 > 50 then
      RECNUMB1 := RECNUMB1-50;
    else
      RECNUMB1 := 1;
    end if;
    pselect;
    poutput;
  elsif ACTION = GBUTTONFWD then     RECNUMP := RECNUMP+1;
    RECNUMB1 := RECNUMB1+50;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     RECNUMP := 1;
    RECNUMB1 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := ''Form is changed.'';
    --end if;
    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>GBUTTONPREV</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p( ''<input type=button class="rasdButton" value="''';
 v_vc(20) := ' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>GBUTTONPREV</rblockid></trigger><trigger><blockid>HINTCONTENT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>HINTCONTENT</rblockid></trigger><trigger><blockid></blockid><triggerid>POST_ACTION</triggerid><plsql><![CDATA[null;
]]></plsql><plsqlspec><![CDATA[  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONBCK, GBUTTONFWD, GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then
    raise_application_error(''-20000'', ''ACTION="''||ACTION||''" is not defined. Define it in POST_ACTION trigger.'');
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B1</blockid><triggerid>POST_SELECT</triggerid><plsql><![CDATA[if B1SOURCEX is not null then



B1ATTS#SET.custom := ''class="showrow"'';



end if;
]]></plsql><plsqlspec><![CDATA[-- Triggers after executing SQL. Use (i__) to access fields values.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigge';
 v_vc(21) := 'r><blockid></blockid><triggerid>POST_SUBMIT</triggerid><plsql><![CDATA[----put procedure in the begining of trigger;

post_submit_template;


]]></plsql><plsqlspec><![CDATA[-- Executing after filling fields on submit.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>_ changes</triggerid><plsql><![CDATA[  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := ''/* Change LOG:

20230301 - Created new 2 version

20170105 - Modified filter on Pages page

*/'';

    return version;

 end;
]]></plsql><plsqlspec><![CDATA[  function changes(p_log out varchar2) return varchar2;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure custom template</triggerid><plsql><![CDATA[function showLabel(plabel varchar2, pcolor varchar2 default ''U'', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,'' '',''''),''.'',''''),lang,replace(this_form,''2'','''')||''_DIALOG'');

end if;



if pcolor is null then



return v__;



else



return ''<font color="''||pcolor||''">''||v__||''</font>'';



end if;





end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure post_submit template</triggerid><plsql><![CDATA[procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text(''User has no privileges to save data!'', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;';
 v_vc(22) := '





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure_compile_template</triggerid><plsql><![CDATA[procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, ''COMPILE_S'', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text(''From is not generated.'', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           ''begin '' || v_server || ''.c_debug := false;''|| v_server || ''.form('' || PFORMID ||

                           '','''''' || lang || '''''');end;'',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text(''From is generated.'', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || ''<br/> - ''||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text(''Form i';
 v_vc(23) := 's generated with compilation error. Check your code.'', lang)||''(''||sqlerrm||'')'';



            else

            sporocilo := RASDI_TRNSLT.text(''Form is NOT generated - internal RASD error.'', lang) || ''(''||sqlerrm||'')<br>''||

                         RASDI_TRNSLT.text(''To debug run: '', lang) || ''begin '' || v_server || ''.form('' || PFORMID ||

                         '','''''' || lang || '''''');end;'' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, ''COMPILE_E'', pcompid);

end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><element>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid><';
 v_vc(24) := '/rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''<%= rasdc_library.formName(PFORMID, LANG) %>''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');
%>]]></value><valuecode><![CDATA['');
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''''|| rasdc_library.formName(PFORMID, LANG) ||''''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');

htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><';
 v_vc(25) := '/forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>RASDC2_HTML</id><nameid>RASDC2_HTML</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value><![CDATA[?]]></value><valuecode><![CDATA[="?"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RASDC2_HTML]]></value><valuecode><![CDATA[="RASDC2_HTML"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></';
 v_vc(26) := 'rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><element>FORM_LAB</element><type>F</type><id>RASDC2_HTML_LAB</id><nameid>RASDC2_HTML_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_LAB]]></value><valuecode><![CDATA[="RASDC2_HTML_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''RASDC2_HTML_LAB'',''<%= rasdc_library.formName(PFORMID, LANG) %>'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''RASDC2_HTML_LAB'',''''|| rasdc_library.formName(PFORMID, LANG) ||'''') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid';
 v_vc(27) := '><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>RASDC2_HTML_MENU</id><nameid>RASDC2_HTML_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_MENU]]></value><valuecode><![CDATA[="RASDC2_HTML_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''RASDC2_HTML_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''RASDC2_HTML_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rl';
 v_vc(28) := 'obid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</elementid><pelementid>15</pelementid><orderby>9999</orderby><element>FORM_BOTTOM</element><type>F</type><id>RASDC2_HTML_BOTTOM</id><nameid>RASDC2_HTML_BOTTOM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBottom]]></value><valuecode><![CDATA[="rasdFormBottom"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_BOTTOM]]></value><valuecode><![CDATA[="RASDC2_HTML_BOTTOM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''RASDC2_HTML_BOTTOM'',1,instr(''RASDC2_HTML_BOTTOM'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''RASDC2_HTML_BOTTOM'',1,instr(''RASDC2_HTML_BOTTOM'', ''_'',-1)-1) , '''') ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn';
 v_vc(29) := '>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>RASDC2_HTML_DIV</id><nameid>RASDC2_HTML_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_DIV]]></value><valuecode><![CDATA[="RASDC2_HTML_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>20</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>RASDC2_HTML_HEAD</id><nameid>RASDC2_HTML_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLA';
 v_vc(30) := 'SS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_HEAD]]></value><valuecode><![CDATA[="RASDC2_HTML_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>20</pelementid><orderby>5</orderby><element>FORM_BODY</element><type>F</type><id>RASDC2_HTML_BODY</id><nameid>RASDC2_HTML_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_BODY]]></value><valuecode><!';
 v_vc(31) := '[CDATA[="RASDC2_HTML_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>20</pelementid><orderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>RASDC2_HTML_FOOTER</id><nameid>RASDC2_HTML_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_FOOTER]]></value><valuecode><![CDATA[="RASDC2_HTML_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></value';
 v_vc(32) := 'id><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>20</pelementid><orderby>2</orderby><element>FORM_RESPONSE</element><type>F</type><id>RASDC2_HTML_RESPONSE</id><nameid>RASDC2_HTML_RESPONSE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormResponse]]></value><valuecode><![CDATA[="rasdFormResponse"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_RESPONSE]]></value><valuecode><![CDATA[="RASDC2_HTML_RESPONSE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><elem';
 v_vc(33) := 'ent><elementid>25</elementid><pelementid>24</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>RASDC2_HTML_MESSAGE</id><nameid>RASDC2_HTML_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="rasdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_MESSAGE]]></value><valuecode><![CDATA[="RASDC2_HTML_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>24</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>RASDC2_HTML_ERROR</id><nameid>RASDC2_HTML_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]';
 v_vc(34) := '></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_ERROR]]></value><valuecode><![CDATA[="RASDC2_HTML_ERROR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>RASDC2_HTML_WARNING</id><nameid>RASDC2_HTML_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_HTML_WARNING]]></value><valuecode><![CDATA[="RASDC2_HTML_WARNING"]]></valuecode><forloop>';
 v_vc(35) := '</forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid><pelementid>22</pelementid><orderby>105</orderby><element>BLOCK_DIV</element><type>B</type><id>P_DIV</id><nameid>P_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_DIV]]></value><valuecode><![CDATA[="P_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</o';
 v_vc(36) := 'rderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockP_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockP_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</elementid><pelementid>28</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelementid>29</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>P_LAB</id><nameid>P_LAB</name';
 v_vc(37) := 'id><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_LAB]]></value><valuecode><![CDATA[="P_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>28</pelementid><orderby>106</orderby><element>TABLE_</element><type></type><id>P_TABLE</id><nameid>P_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text><';
 v_vc(38) := '/text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_TABLE_RASD]]></value><valuecode><![CDATA[="P_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>31</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>P_BLOCK</id><nameid>P_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_BLOCK_NAME]]></value><valuecode><![CDATA[="P_BLOCK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn';
 v_vc(39) := '>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>31</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>P_THEAD</id><nameid>P_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelementid>33</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDA';
 v_vc(40) := 'TA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>35</elementid><pelementid>34</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>PFELEMENTID_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPFELEMENTID]]></value><valuecode><![CDATA[="rasdTxLabPFELEMENTID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>35</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>PFELEMENTID_LAB</id><nameid>PFELEMENTID_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform><';
 v_vc(41) := '/rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFELEMENTID_LAB]]></value><valuecode><![CDATA[="PFELEMENTID_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>34</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>PTCLEVEL_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><value';
 v_vc(42) := 'code><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPTCLEVEL]]></value><valuecode><![CDATA[="rasdTxLabPTCLEVEL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>37</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>PTCLEVEL_LAB</id><nameid>PTCLEVEL_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PTCLEVEL_LAB]]></value><valuecode><![CDATA[="PTCLEVEL_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid>';
 v_vc(43) := '<textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>34</pelementid><orderby>3</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSRC_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGBTNSRC]]></value><valuecode><![CDATA[="rasdTxLabPGBTNSRC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CD';
 v_vc(44) := 'ATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>39</pelementid><orderby>3</orderby><element>FONT_</element><type>L</type><id>PGBTNSRC_LAB</id><nameid>PGBTNSRC_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSRC_LAB]]></value><valuecode><![CDATA[="PGBTNSRC_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid';
 v_vc(45) := '></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>41</elementid><pelementid>34</pelementid><orderby>4</orderby><element>TX_</element><type>E</type><id></id><nameid>PUPLOAD_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPUPLOAD]]></value><valuecode><![CDATA[="rasdTxLabPUPLOAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>42</elementid><pelementid>41</pelementid><orderby>4</orderby><ele';
 v_vc(46) := 'ment>FONT_</element><type>L</type><id>PUPLOAD_LAB</id><nameid>PUPLOAD_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PUPLOAD_LAB]]></value><valuecode><![CDATA[="PUPLOAD_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>43</elementid><pelementid>34</pelementid><orderby>5</orderby><element>TX_</element><type>E</type><id></id><nameid>PDOWNLOAD_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><a';
 v_vc(47) := 'ttribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPDOWNLOAD]]></value><valuecode><![CDATA[="rasdTxLabPDOWNLOAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>44</elementid><pelementid>43</pelementid><orderby>5</orderby><element>FONT_</element><type>L</type><id>PDOWNLOAD_LAB</id><nameid>PDOWNLOAD_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value>';
 v_vc(48) := '<![CDATA[PDOWNLOAD_LAB]]></value><valuecode><![CDATA[="PDOWNLOAD_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>45</elementid><pelementid>22</pelementid><orderby>115</orderby><element>BLOCK_DIV</element><type>B</type><id>B1_DIV</id><nameid>B1_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_DIV]]></value><valuecode><![CDATA[="B1_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><';
 v_vc(49) := 'rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB1_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB1_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>46</elementid><pelementid>45</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><tex';
 v_vc(50) := 't></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>47</elementid><pelementid>46</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B1_LAB</id><nameid>B1_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_LAB]]></value><valuecode><![CDATA[="B1_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></att';
 v_vc(51) := 'ribute></attributes></element><element><elementid>48</elementid><pelementid>45</pelementid><orderby>116</orderby><element>TABLE_N</element><type></type><id>B1_TABLE</id><nameid>B1_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_TABLE_RASD]]></value><valuecode><![CDATA[="B1_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>49</elementid><pelementid>48</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B1';
 v_vc(52) := '_BLOCK</id><nameid>B1_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_BLOCK_NAME]]></value><valuecode><![CDATA[="B1_BLOCK_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB1 in 1..B1RID.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>50</elementid><pelementid>48</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B1_THEAD</id><nameid>B1_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></t';
 v_vc(53) := 'extid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>51</elementid><pelementid>50</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>52</elementid><pelementid>51</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B1ATTS_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB1]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB1ATTS]]></value><valuecode><![CDATA[="rasdTxLabB1ATTS"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><valu';
 v_vc(54) := 'e><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>53</elementid><pelementid>52</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B1ATTS_LAB</id><nameid>B1ATTS_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1ATTS_LAB]]></value><valuecode><![CDATA[="B1ATTS_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode>';
 v_vc(55) := '<rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>54</elementid><pelementid>51</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B1ELEMENT_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB1]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB1ELEMENT]]></value><valuecode><![CDATA[="rasdTxLabB1ELEMENT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>55</elementid><pelementid>54</pelementid><orderby>2</orderby><element>FONT_</element><ty';
 v_vc(56) := 'pe>L</type><id>B1ELEMENT_LAB</id><nameid>B1ELEMENT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1ELEMENT_LAB]]></value><valuecode><![CDATA[="B1ELEMENT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Element'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Element'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>56</elementid><pelementid>51</pelementid><orderby>3</orderby><element>TX_</element><type>E</type><id></id><nameid>B1HIDDENYN_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid';
 v_vc(57) := '></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB1]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB1HIDDENYN]]></value><valuecode><![CDATA[="rasdTxLabB1HIDDENYN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>57</elementid><pelementid>56</pelementid><orderby>3</orderby><element>FONT_</element><type>L</type><id>B1HIDDENYN_LAB</id><nameid>B1HIDDENYN_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attrib';
 v_vc(58) := 'ute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1HIDDENYN_LAB]]></value><valuecode><![CDATA[="B1HIDDENYN_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Hidden'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Hidden'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>58</elementid><pelementid>51</pelementid><orderby>4</orderby><element>TX_</element><type>E</type><id></id><nameid>B1SOURCE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB1]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB1SOU';
 v_vc(59) := 'RCE]]></value><valuecode><![CDATA[="rasdTxLabB1SOURCE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>59</elementid><pelementid>58</pelementid><orderby>4</orderby><element>FONT_</element><type>L</type><id>B1SOURCE_LAB</id><nameid>B1SOURCE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1SOURCE_LAB]]></value><valuecode><![CDATA[="B1SOURCE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><tex';
 v_vc(60) := 'tcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Source'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Source'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>60</elementid><pelementid>51</pelementid><orderby>5</orderby><element>TX_</element><type>E</type><id></id><nameid>B1REFERENCE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB1]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB1REFERENCE]]></value><valuecode><![CDATA[="rasdTxLabB1REFERENCE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby';
 v_vc(61) := '>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>61</elementid><pelementid>60</pelementid><orderby>5</orderby><element>FONT_</element><type>L</type><id>B1REFERENCE_LAB</id><nameid>B1REFERENCE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1REFERENCE_LAB]]></value><valuecode><![CDATA[="B1REFERENCE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Ref. to id'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(';
 v_vc(62) := '''Ref. to id'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>62</elementid><pelementid>21</pelementid><orderby>0</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMP</id><nameid>RECNUMP</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMP_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMP_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMP_NAME]]></value><valuecode><![CDATA[="RECNUMP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></r';
 v_vc(63) := 'form><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMP_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMP))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>63</elementid><pelementid>21</pelementid><orderby>0</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB10</id><nameid>RECNUMB10</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]';
 v_vc(64) := '></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB10_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB10_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB10_NAME]]></value><valuecode><![CDATA[="RECNUMB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform>';
 v_vc(65) := '<rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB10_VALUE]]></value><valuecode><![CDATA[="''||RECNUMB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>64</elementid><pelementid>21</pelementid><orderby>0</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB1</id><nameid>RECNUMB1</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB1_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB1_RASD"]]></valuecode><fo';
 v_vc(66) := 'rloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB1_NAME]]></value><valuecode><![CDATA[="RECNUMB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB1_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMB1))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribu';
 v_vc(67) := 'te><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>65</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>ACTION</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><e';
 v_vc(68) := 'ndloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><valuecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>66</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PAGE</id><nameid>PAGE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</';
 v_vc(69) := 'type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PAGE_NAME_RASD]]></value><valuecode><![CDATA[="PAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PAGE_NAME]]></value><valuecode><![CDATA[="PAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidden';
 v_vc(70) := 'yn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PAGE_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(PAGE))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>67</elementid><pelementid>21</pelementid><orderby>6</orderby><element>INPUT_HIDDEN</element><type>D</type><id>LANG</id><nameid>LANG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><!';
 v_vc(71) := '[CDATA[LANG_NAME_RASD]]></value><valuecode><![CDATA[="LANG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[LANG_NAME]]></value><valuecode><![CDATA[="LANG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[LANG_VALUE]]></value><valuecode><![CDATA[="''||LANG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><r';
 v_vc(72) := 'lobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>68</elementid><pelementid>21</pelementid><orderby>7</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PFORMID</id><nameid>PFORMID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFORMID_NAME_RASD]]></value><valuecode><![CDATA[="PFORMID_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFORMID_NAME]]></value><valuecode><![CDATA[="PFORMID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><va';
 v_vc(73) := 'lue></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PFORMID_VALUE]]></value><valuecode><![CDATA[="''||PFORMID||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>69</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>INPUT_HIDDEN</element><type>D</type><id>POLJE</id><nameid>POLJE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attr';
 v_vc(74) := 'ibute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[POLJE_NAME_RASD]]></value><valuecode><![CDATA[="POLJE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[POLJE_NAME]]></value><valuecode><![CDATA[="POLJE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forl';
 v_vc(75) := 'oop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[POLJE_VALUE]]></value><valuecode><![CDATA[="''||POLJE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>70</elementid><pelementid>21</pelementid><orderby>3</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONBCK</id><nameid>GBUTTONBCK</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID';
 v_vc(76) := '</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONBCK_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONBCK_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONBCK_NAME]]></value><valuecode><![CDATA[="GBUTTONBCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONBCK_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONBCK||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>';
 v_vc(77) := 'E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONBCK  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>71</elementid><pelementid>23</pelementid><orderby>10001</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONBCK</id><nameid>GBUTTONBCK</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONBCK_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONBCK_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONBCK_NAME]]></value><valuecode><![CDATA[="GBUTTONBCK"]]></valuecode><forl';
 v_vc(78) := 'oop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONBCK_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONBCK||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONBCK  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source';
 v_vc(79) := '><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>72</elementid><pelementid>21</pelementid><orderby>4</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONFWD</id><nameid>GBUTTONFWD</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONFWD_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONFWD_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONFWD_NAME]]></value><valuecode><![CDATA[="GBUTTONFWD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></text';
 v_vc(80) := 'code><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONFWD_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONFWD||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONFWD  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>73</elementid><pelementid>23</pelementid><orderby>10002</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONFWD</id><nameid>GBUTTONFWD</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></';
 v_vc(81) := 'text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONFWD_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONFWD_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONFWD_NAME]]></value><valuecode><![CDATA[="GBUTTONFWD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><v';
 v_vc(82) := 'aluecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONFWD_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONFWD||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONFWD  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>74</elementid><pelementid>21</pelementid><orderby>13</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButto';
 v_vc(83) := 'n'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[O';
 v_vc(84) := 'NDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rl';
 v_vc(85) := 'obid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode';
 v_vc(86) := '><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>75</elementid><pelementid>23</pelementid><orderby>10011</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]><';
 v_vc(87) := '/name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="';
 v_vc(88) := '''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></val';
 v_vc(89) := 'uecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>76</elementid><pelementid>21</pelementid><orderby>14</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endta';
 v_vc(90) := 'gelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><va';
 v_vc(91) := 'lue><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMP';
 v_vc(92) := 'ILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><r';
 v_vc(93) := 'lobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>77</elementid><pelementid>23</pelementid><orderby>10012</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''C';
 v_vc(94) := 'LASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid>';
 v_vc(95) := '</rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''';
 v_vc(96) := '); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><';
 v_vc(97) := 'type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>78</elementid><pelementid>21</pelementid><orderby>15</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><texti';
 v_vc(98) := 'd></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><n';
 v_vc(99) := 'ame><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></v';
 v_vc(100) := 'alue><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>79</elementid><pelementid>23</pelementid><orderby>10013</orderby><element>INPUT_RESET</element><type>D</type><id';
 v_vc(101) := '>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><';
 v_vc(102) := '![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonl';
 v_vc(103) := 'y then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<';
 v_vc(104) := '% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>80</elementid><pelementid>21</pelementid><orderby>16</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribut';
 v_vc(105) := 'e><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></so';
 v_vc(106) := 'urce><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                ';
 v_vc(107) := '                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>81</elementid><pelementid>23</pelementid><orderby>10014</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p(''';
 v_vc(108) := ' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><t';
 v_vc(109) := 'ext></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>82</elementid><pelementid>21</pelementid><orderby>18</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONDELEL</id><nameid>GBUTTONDELEL</nameid><endtagelementid></endtagelementid><source></source><hidd';
 v_vc(110) := 'enyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONDELEL_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONDELEL_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONDELEL_NAME]]></value><valuecode><![CDATA[="GBUTTONDELEL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><r';
 v_vc(111) := 'form></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONDELEL_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONDELEL||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONDELEL  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>83</elementid><pelementid>23</pelementid><orderby>10016</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONDELEL</id><nameid>GBUTTONDELEL</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute>';
 v_vc(112) := '<type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONDELEL_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONDELEL_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONDELEL_NAME]]></value><valuecode><![CDATA[="GBUTTONDELEL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CD';
 v_vc(113) := 'ATA[GBUTTONDELEL_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONDELEL||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONDELEL  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>84</elementid><pelementid>26</pelementid><orderby>10046</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid><';
 v_vc(114) := '/valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop';
 v_vc(115) := '><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>85</elementid><pelementid>25</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLIC';
 v_vc(116) := 'K</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlob';
 v_vc(117) := 'id></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>86</elementid><pelementid>27</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloo';
 v_vc(118) := 'p></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WARNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>87</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name>';
 v_vc(119) := '<![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>88</elementid><pelementid>23</pelementid><orderby>300874</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><o';
 v_vc(120) := 'rderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>89</elementid><pelementid>32</pelementid><orderby>180</orderby><element>TX_</element><type>E</type><id></id><nameid>PFELEMENTID_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPFELEMENTID rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPFELEMENTID rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPFELEMENTID_NAME]]></value><valuecode><![CDATA[="rasdTxPFELEMENTID_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></t';
 v_vc(121) := 'extid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>90</elementid><pelementid>89</pelementid><orderby>1</orderby><element>SELECT_</element><type>D</type><id>PFELEMENTID</id><nameid>PFELEMENTID</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdSelect ]]></value><valuecode><![CDATA[="rasdSelect "]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PFELEMENTID_NAME_RASD]]></value><valuecode><![CDATA[="PFELEMENTID_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA';
 v_vc(122) := '[name]]></name><value><![CDATA[PFELEMENTID_NAME]]></value><valuecode><![CDATA[="PFELEMENTID_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</select]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<select]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></r';
 v_vc(123) := 'form><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[lov$elementi]]></value><valuecode><![CDATA[''); js_Slov$elementi(PFELEMENTID(1),''PFELEMENTID_1'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>91</elementid><pelementid>32</pelementid><orderby>210</orderby><element>TX_</element><type>E</type><id></id><nameid>PTCLEVEL_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPTCLEVEL rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPTCLEVEL rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPTCLEVEL_NAME]]></value><valuecode><![CDATA[="rasdTxPTCLEVEL_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>92</elementid><pel';
 v_vc(124) := 'ementid>91</pelementid><orderby>1</orderby><element>SELECT_</element><type>D</type><id>PTCLEVEL</id><nameid>PTCLEVEL</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdSelect ]]></value><valuecode><![CDATA[="rasdSelect "]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PTCLEVEL_NAME_RASD]]></value><valuecode><![CDATA[="PTCLEVEL_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PTCLEVEL_NAME]]></value><valuecode><![CDATA[="PTCLEVEL_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn>';
 v_vc(125) := '<valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</select]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<select]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[lov$tclevel]]></value><valuecode><![CDATA[''); js_Slov$tclevel(PTCLEVEL(1),''PTCLEVEL_1'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>93</elementid><pelementid>32</pelementid><orderby>240</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSRC_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></';
 v_vc(126) := 'name><value><![CDATA[rasdTxPGBTNSRC rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGBTNSRC rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGBTNSRC_NAME]]></value><valuecode><![CDATA[="rasdTxPGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>94</elementid><pelementid>93</pelementid><orderby>1</orderby><element>INPUT_SUBMIT</element><type>D</type><id>PGBTNSRC</id><nameid>PGBTNSRC</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</h';
 v_vc(127) := 'iddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSRC_NAME_RASD]]></value><valuecode><![CDATA[="PGBTNSRC_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PGBTNSRC_NAME]]></value><valuecode><![CDATA[="PGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PGBTNSRC_VALUE]]></value><valuecode><![CDATA[="''||PGBTNSRC(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></val';
 v_vc(128) := 'ueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>95</elementid><pelementid>32</pelementid><orderby>280</orderby><element>TX_</element><type>E</type><id></id><nameid>PUPLOAD_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPUPLOAD rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPUPLOAD rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPUPLOAD_NAME]]></value><valuecode><![CDATA[="rasdTxPUPLOAD_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><!';
 v_vc(129) := '[CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>96</elementid><pelementid>95</pelementid><orderby>1</orderby><element>IMG_</element><type>D</type><id>PUPLOAD</id><nameid>PUPLOAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>11</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[rasdImg]]></value><valuecode><![CDATA[="rasdImg"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>9</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PUPLOAD_NAME_RASD]]></value><valuecode><![CDATA[="PUPLOAD_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[link$upload]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$upload(PUPLOAD(1),''PUPLOAD_1'');
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,h';
 v_vc(130) := 'eight=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>A_SRC</attribute><type>A</type><text></text><name><![CDATA[SRC]]></name><value><![CDATA[PUPLOAD_VALUE]]></value><valuecode><![CDATA[="''||PUPLOAD(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  PUPLOAD_VALUE is not null and ''<%= RASDI_TRNSLT.text(''Upload'',LANG) %>'' is not null  then htp.prn(''title="<%= RASDI_TRNSLT.text(''Upload'',LANG) %>"''); end if; %>]]></value><valuecode><![CDATA['');  if  PUPLOAD(1) is not null and ''''|| RASDI_TRNSLT.text(''Upload'',LANG) ||'''' is not null  then htp.prn(''title="''|| RASDI_TRNSLT.text(''Upload'',LANG) ||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</IMG]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1<';
 v_vc(131) := '/orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<IMG]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>97</elementid><pelementid>32</pelementid><orderby>300</orderby><element>TX_</element><type>E</type><id></id><nameid>PDOWNLOAD_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPDOWNLOAD rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPDOWNLOAD rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPDOWNLOAD_NAME]]></value><valuecode><![CDATA[="rasdTxPDOWNLOAD_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>98</elementid><pelementid>97</pelementid><orderby>1</orderby><element>IMG_</element><type>D</type><id>PDOWNLOAD</id><namei';
 v_vc(132) := 'd>PDOWNLOAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>11</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[rasdImg]]></value><valuecode><![CDATA[="rasdImg"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>9</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PDOWNLOAD_NAME_RASD]]></value><valuecode><![CDATA[="PDOWNLOAD_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[link$DOWNLOAD]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$DOWNLOAD(PDOWNLOAD(1),''PDOWNLOAD_1'');
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop>';
 v_vc(133) := '<source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>A_SRC</attribute><type>A</type><text></text><name><![CDATA[SRC]]></name><value><![CDATA[PDOWNLOAD_VALUE]]></value><valuecode><![CDATA[="''||PDOWNLOAD(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  PDOWNLOAD_VALUE is not null and ''<%= RASDI_TRNSLT.text(''Download'',LANG) %>'' is not null  then htp.prn(''title="<%= RASDI_TRNSLT.text(''Download'',LANG) %>"''); end if; %>]]></value><valuecode><![CDATA['');  if  PDOWNLOAD(1) is not null and ''''|| RASDI_TRNSLT.text(''Download'',LANG) ||'''' is not null  then htp.prn(''title="''|| RASDI_TRNSLT.text(''Download'',LANG) ||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</IMG]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<IMG]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>99</elementid><pelem';
 v_vc(134) := 'entid>49</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>B1</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[<%=iB1%>]]></value><valuecode><![CDATA[="''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source';
 v_vc(135) := '><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>100</elementid><pelementid>99</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B1RS</id><nameid>B1RS</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1RS_NAME_RASD]]></value><valuecode><![CDATA[="B1RS_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1RS_NAME]]></value><valuecode><![CDATA[="B1RS_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attr';
 v_vc(136) := 'ibute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B1RS_VALUE]]></value><valuecode><![CDATA[="''||B1RS(iB1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>101</elementid><pelementid>99</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B1RID</id><nameid>B1RID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><te';
 v_vc(137) := 'xtid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1RID_NAME_RASD]]></value><valuecode><![CDATA[="B1RID_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1RID_NAME]]></value><valuecode><![CDATA[="B1RID_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></te';
 v_vc(138) := 'xt><name><![CDATA[value]]></name><value><![CDATA[B1RID_VALUE]]></value><valuecode><![CDATA[="''||to_char(B1RID(iB1))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>102</elementid><pelementid>99</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B1ELEMENTID</id><nameid>B1ELEMENTID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1ELEMENTID_NAME_RASD]]></value><valuecode><![CDATA[="B1ELEMENTID_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valu';
 v_vc(139) := 'eid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1ELEMENTID_NAME]]></value><valuecode><![CDATA[="B1ELEMENTID_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B1ELEMENTID_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(B1ELEMENTID(iB1)))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</att';
 v_vc(140) := 'ribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>103</elementid><pelementid>99</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B1ELEMENTX</id><nameid>B1ELEMENTX</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1ELEMENTX_NAME_RASD]]></value><valuecode><![CDATA[="B1ELEMENTX_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1ELEMENTX_NAME]]></value><valuecode><![CDATA[="B1ELEMENTX_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop>';
 v_vc(141) := '</endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B1ELEMENTX_VALUE]]></value><valuecode><![CDATA[="''||B1ELEMENTX(iB1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>104</elementid><pelementid>49</pelementid><orderby>2004</orderby><element>TX_</element><type>E</type><id></id><nameid>B1ATTS_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>';
 v_vc(142) := 'A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB1ATTS rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB1ATTS rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB1ATTS_NAME]]></value><valuecode><![CDATA[="rasdTxB1ATTS_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>105</elementid><pelementid>104</pelementid><orderby>1</orderby><element>A_</element><type>D</type><id>B1ATTS</id><nameid>B1ATTS</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[href]]></name><value><![CDATA[link$attributs]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$attributs(B1ATTS(iB1),''B1ATTS_''||iB1||'''');
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby>';
 v_vc(143) := '<attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1ATTS_NAME_RASD]]></value><valuecode><![CDATA[="B1ATTS_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1ATTS_NAME]]></value><valuecode><![CDATA[="B1ATTS_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  B1ATTS_VALUE is not null and ''<%= RASDI_TRNSLT.text(''Attributes of element'',LANG) %>'' is not null  then htp.prn(''title="<%= RASDI_TRNSLT.text(''Attributes of element'',LANG) %>"''); end if; %>]]></value><valuecode><![CDATA['');  if  B1ATTS(iB1) is not null and ''''|| RASDI_TRNSLT.text(''Attributes of element'',LANG) ||'''' is not null  then htp.prn(''title="''|| RASDI_TRNSLT.text(''Attributes of element'',LANG) ||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value></value><valuecode></valuecode><forloop></forlo';
 v_vc(144) := 'op><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B1ATTS#SET(iB1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B1ATTS#SET(iB1).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B1ATTS#SET(iB1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B1ATTS#SET(iB1).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B1ATTS#SET(iB1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B1ATTS#SET(iB1).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid>';
 v_vc(145) := '</attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B1ATTS#SET(iB1).info is not null then htp.p('' title="''||B1ATTS#SET(iB1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B1ATTS#SET(iB1).info is not null then htp.p('' title="''||B1ATTS#SET(iB1).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B1ATTS#SET(iB1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B1ATTS#SET(iB1).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B1ATTS#SET(iB1).error is not null then htp.p('' title="''||B1ATTS#SET(iB1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B1ATTS#SET(iB1).error is not null then htp.p('' title="''||B1ATTS#SET(iB1).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</a><% end if; %>]]></value><valuecode><![CDATA[</a>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B1ATTS#SET(iB1).visible then %><a]]></value><valuecode><![CDATA['');  if B1ATTS#SET(iB1).visible then
htp.prn(''<a]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></att';
 v_vc(146) := 'ribute><attribute><orderby>12</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B1ATTS_VALUE]]></value><valuecode><![CDATA[''||B1ATTS(iB1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>106</elementid><pelementid>49</pelementid><orderby>2006</orderby><element>TX_</element><type>E</type><id></id><nameid>B1ELEMENT_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB1ELEMENT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB1ELEMENT rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB1ELEMENT_NAME]]></value><valuecode><![CDATA[="rasdTxB1ELEMENT_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>107</elementid><pelementid>106</pelementid><orderby>1</orderby><element';
 v_vc(147) := '>FONT_</element><type>D</type><id>B1ELEMENT</id><nameid>B1ELEMENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1ELEMENT_NAME_RASD]]></value><valuecode><![CDATA[="B1ELEMENT_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></r';
 v_vc(148) := 'id></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B1ELEMENT_VALUE]]></value><valuecode><![CDATA[''||B1ELEMENT(iB1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>108</elementid><pelementid>49</pelementid><orderby>2020</orderby><element>TX_</element><type>E</type><id></id><nameid>B1HIDDENYN_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB1HIDDENYN rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB1HIDDENYN rasdTxTypeC"]]></valuecode><for';
 v_vc(149) := 'loop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB1HIDDENYN_NAME]]></value><valuecode><![CDATA[="rasdTxB1HIDDENYN_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>109</elementid><pelementid>108</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B1HIDDENYN</id><nameid>B1HIDDENYN</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox ]]></value><valuecode><![CDATA[="rasdCheckbox "]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><ri';
 v_vc(150) := 'd></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1HIDDENYN_NAME_RASD]]></value><valuecode><![CDATA[="B1HIDDENYN_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1HIDDENYN_NAME]]></value><valuecode><![CDATA[="B1HIDDENYN_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B1HIDDENYN_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B1HIDDENYN_''||iB1||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B1HIDDENYN_VALUE]]></value><valuecode><![CDATA[="''||B1HIDDENYN(iB1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><';
 v_vc(151) := 'textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B1HIDDENYN_VALUE'''' , ''''B1HIDDENYN_NAME'''' ); }) </SCRIPT>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B1HIDDENYN(iB1)||'''''' , ''''B1HIDDENYN_''||iB1||'''''' ); }) </SCRIPT>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>110</elementid><pelementid>49</pelementid><orderby>2040</orderby><element>TX_</element><type>E</type><id></id><nameid>B1SOURCE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB1SOURCE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB1SOURCE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB1SOURCE_NAME]]></value><valuecode><![CDATA[="rasdTxB1SOURCE_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</';
 v_vc(152) := 'td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>111</elementid><pelementid>110</pelementid><orderby>1</orderby><element>SELECT_</element><type>D</type><id>B1SOURCE</id><nameid>B1SOURCE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdSelect ]]></value><valuecode><![CDATA[="rasdSelect "]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[B1SOURCE_NAME_RASD]]></value><valuecode><![CDATA[="B1SOURCE_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B1SOURCE_NAME]]></value><valuecode><![CDATA[="B1SOURCE_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidde';
 v_vc(153) := 'nyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</select]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<select]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[lov$source]]></value><valuecode><![CDATA[';
 v_vc(154) := '''); js_Slov$source(B1SOURCE(iB1),''B1SOURCE_''||iB1||'''');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>112</elementid><pelementid>49</pelementid><orderby>2060</orderby><element>TX_</element><type>E</type><id></id><nameid>B1REFERENCE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB1REFERENCE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB1REFERENCE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB1REFERENCE_NAME]]></value><valuecode><![CDATA[="rasdTxB1REFERENCE_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>113</elementid><pelementid>112</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B1REFERENCE</id><nameid>B1REFERENCE</nameid><endtagelementid>0</endtag';
 v_vc(155) := 'elementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1REFERENCE_NAME_RASD]]></value><valuecode><![CDATA[="B1REFERENCE_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text><';
 v_vc(156) := '/text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B1REFERENCE_VALUE]]></value><valuecode><![CDATA[''||B1REFERENCE(iB1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASDC2_HTML_v.1.1.20240228090934.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end RASDC2_HTML;

/
